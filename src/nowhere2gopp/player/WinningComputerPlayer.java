package nowhere2gopp.player;

import java.util.*;
import nowhere2gopp.board.HexagonalBoard.GamePhase;
import nowhere2gopp.preset.*;
import java.io.Serializable;
import nowhere2gopp.base.NowhereToGoe;


/**
 * @author Titus Schlotthauer
 * @since 2019-06-25
 * @version 1.4
 * This class represents a fast and intelligent Computer that tries it's utmost best to win
 * To achieve this the Computer Calculates iteratively, by checking certain conditions for specific moves
 * or rating each Site that the Computer or Enemy can reach and according to that decides on a move
 */
public class WinningComputerPlayer extends BasePlayer implements Serializable{

	public static final long serialVersionUID = 2019071453L;
	
	/*
	 * This function is where the Computer decides on and executes a Move that will be most optimal
	 */
	@Override
	public Move request() throws Exception{
	
		if(pfcs != PlFuncCallState.request && NowhereToGoe.DEBUG){	//Checks if the Player function call state is correctly aligned with this request
			throw new IllegalStateException("Tried to request move in state " + board.getStatus());
		}

		if(board.getStatus() != Status.Ok && NowhereToGoe.DEBUG){	//Checks if the boardstatus is correctly aligned with this request 
			throw new IllegalStateException("Tried to request move in state " + board.getStatus());
		}
		
		Move next = new Move(MoveType.End);	//Preemptively set Move so that if the computer can't make/find a Move we already have a set the correct proxy
		
		Site agent = board.getAgent(color);	//Get the current Site the "agent" or computer is on by using the color that was assigned to this computer
		Site enemy = board.getAgent(oppColor);	//Get the current Site the "enemy" is on by using the color that was assigned to the opposing computer
		Collection<SiteSet> links = new ArrayList<>();	//Retrieve all links from the proxy board the computer has inherited through BasePlayer
		links.addAll(board.getLinks());

		
		if(board.getPhase() == GamePhase.LinkLink){	//If the current Phase is the beginning Phase (remove 2 Links per Turn)
			SiteSet toRemove1 = null;		//toRemove1 represents the first link that is to be removed
			SiteSet toRemove2 = null;		//toRemove2 represents the second link that is to be removed

			while (toRemove1 == null || toRemove2 == null) {	//Go through all SiteSets/links until both toRemove1 and toRemove2 have been set
				for (SiteSet s : board.getLinks()) {		//While going through all Sitesets/links check
										// if a random value is above a certain threshold and make sure 
										// that toRemove_ hasn't been set before and doesn't equal the other link
					if (Math.random() > 0.99 && toRemove1 == null && s != toRemove2){
						toRemove1 = s;
					}else if(Math.random() > 0.99 && toRemove2 == null && s != toRemove1){
						toRemove2 = s;
					}
				}
			}

			next = new Move(toRemove1, toRemove2);	//Finalize the move by creating a move with both toRemove1 and toRemove2
			
		
	        }else if(board.getPhase() == GamePhase.SetAgent){	//If the current Phase is the Phase where you set your Agent on the Board (and move)
	        	if(color == PlayerColor.Blue && isKillMoveAGo(links, agent, enemy)){	//Check if the computer can "kill" the enemy Player
	        										// if the Playercolor ist Blue since red has already set his Agent by then
	        		next = KillMove(links, agent, enemy);	//If possible conduct that move
	        	}else{
				Site agentSet = null, tmpMove = null;	//agentSet stands for the finalized Site the agent will move to, tmpMove for a temporary proxy
				double csize = 0, ksize = 0;		//csize stands for a temporary proxy for a RadiusValue,
									// ksize stands for the largest current RadiusValue possible
				Move tmpCheck = null;			//tmpCheck checks if there is a possible KillierMove if the Computer has the Playercolor blue
				if(color == PlayerColor.Blue){		//Check the Playercolor
					tmpCheck = KillierMove(links, enemy, enemy);
				}
				if(tmpCheck == null){		//if tmpCheck hasn't been set or couldn't find a move
					for(SiteSet g : links){		//Chech all links for a possible good Site
						//If the Playercolor is blue check the RadiusValue inlcuding enemy since Red has already set his agent
						//Always compare both sides RadiusValue of a link to check which Site to pick
						if(getRadiusValue(links, g.getFirst(), enemy) > getRadiusValue(links, g.getSecond(), enemy) && color == PlayerColor.Blue){
							tmpMove = g.getFirst();
						}else if(getRadiusValue(links, g.getFirst(), enemy) < getRadiusValue(links, g.getSecond(), enemy) && color == PlayerColor.Blue)	{
							tmpMove = g.getSecond();
						}else if(getRadiusValue(links, g.getFirst(), enemy) < getRadiusValue(links, g.getSecond(), enemy)){
							tmpMove = g.getSecond();
						}else{
							tmpMove = g.getFirst();
						}
						if(color == PlayerColor.Blue){		//Again if the Playercolor is blue check the RadiusValue
											// under consideration of the enemy's agent
							csize = getRadiusValue(links, tmpMove, enemy);
						}else{					//Otherwise just check the radiusValue
							csize = getRadiusValue(links, tmpMove, tmpMove);
						}
								
						if(csize > ksize && tmpMove != enemy){		//If the RadiusValue is better than the current RadiusValue, take this Site
							agentSet = tmpMove;
							ksize = csize;
						}
					}
				}else{		//If tmpCheck could be set create a new move where the only difference to the tmpCheck is that we set the first element to agent
					next = new Move(new SiteTuple(agent, tmpCheck.getAgent().getSecond()), tmpCheck.getLink());
				}
				
				SiteSet toRemove = null;	//toRemove stands for the SiteSet/link that is going to be removed
				
				if(color == PlayerColor.Red){	//If the computer is Player-one/the red Player remove a random SiteSet/link that if possible is not near the Player
					while(toRemove == null){
						for(SiteSet s : board.getLinks()){
							Collection<SiteSet> exclude = getReachableLinks(links, agentSet, agentSet);
							double tmp = Math.random();
							if(tmp > 0.99 && !(exclude.contains(s)) || tmp > 0.99 && exclude.containsAll(links) && s.getFirst() != agentSet && s.getSecond() != agentSet){
								toRemove = s;
							}
						}
					}
				}else if(tmpCheck == null){	//If the computer is Player-two/ the blue Player try to remove a SiteSet/link
								// that can hinder the enemy Player since Red has already placed his agent
					toRemove = bestLinkToRemove(links, agentSet, enemy);
				}
				if(agent != null && agentSet != null && toRemove != null && tmpCheck == null){	//If the computer could find a Site to move to,
														// SiteSet/link to remove
						next = new Move(new SiteTuple(agent, agentSet), toRemove);
				}
			}
	        }else if(board.getPhase() == GamePhase.AgentLink){	//If the current Phase is the normal Play Phase, move your Agent and remove a link
	        	if(isKillMoveAGo(links, agent, enemy)){		//Check if the computer can "kill" the enemy Player
	        		next = KillMove(links, agent, enemy);	//If possible conduct that move
	        	}else{
	        		Move BlockMove = KillierMove(links, agent, enemy);	//Else check if the computer can block some movement of the enemy Player efficently
	        		
	        		if(BlockMove == null){		//If there were no Moves found that could have blocked the enemy Player's movements
	        		
					Site agentMove = getBestSite(links, agent, enemy);	//Find the otherwise best the Field to move to for the Computer

					SiteSet toRemove = bestLinkToRemove(links, agentMove, enemy);	//Find the otherwise best SiteSet/link to remove for the Computer
		
					if(agentMove != null && toRemove != null){	//If the computer could find a Site to move to, SiteSet/link to remove
						next = new Move(new SiteTuple(agent, agentMove), toRemove);
					}
				}else{			//If there was a Move found that could block the enemy Player's movements
					next = BlockMove;
				}
			}
		}
		if(next.getType() != MoveType.End){	//Prints the move the computer makes while the move 
			System.out.println("wp : " + color + "\t" + next);
			
		}
				
		board.make(next);		//Convey the Move constructed from the computer to the proxy board

		pfcs = PlFuncCallState.confirm;	//Set the Player Function Call State to confirm since we have now finished making a Move
		
		if(next.getType() == MoveType.End){	//If the Computer lost / couldn't find any possible Moves to make print a quick message
							// that he lost (No possible Move -> This Player lost)
			System.out.println("WinComp lost " + board.getStatus());
		}
		
		return next;	// return the final Move so that the actual Board can process it
	}

	/**
	 * This function calctulates the best link to remove for the Computer by taking various options and checking each of them
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agentMove This Parameter stores the Position where the Player will be moving to
	 * @param enemy This Parameter stores the current Position of the enemy Player
	 * @return This function returns the SiteSet/Link between two Sites that is most optimal for this Computer to remove
	 */
	public static SiteSet bestLinkToRemove(Collection<SiteSet> links, Site agentMove, Site enemy){
		SiteSet toRemove = null;			//The final SiteSet that is going to be removed
		
		Collection<Site> reachable = getNextSites(links, enemy, agentMove);	//We remove links around the enemy since it is the best option if no other functions apply
		for(Site s : reachable){	//Go through all neighbors of the enemies Site
			if(s != agentMove){	//Check that we hinder no movement of our own
				toRemove = new SiteSet(s, enemy);
			}
		}
		
		return toRemove;
	}
	
	/**
	 * This function tries to search for the best Site to move to 
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agentMove This Parameter is a Site from where we calculate the best field to move to
	 * @param enemy This Parameter stores the current Position of the Site "agentMove" can't pass
	 * @return This function returns a Site that is most optimal for this Computer to move to
	 */
	public static Site getBestSite(Collection<SiteSet> links, Site agentMove, Site enemy){
		Collection<Site> reachable = getReachable(links, agentMove, enemy);
		double csize = 0;		//csize represents the current Radius value of theSite, nextSize represents the current Size of how many neighbors the current theSite has 
		Site theSite = null;			//theSite represents the final best Site to move to
		for(Site s : reachable){
			double temp = getRadiusValue(links, s, enemy);		//temp stands for the RadiusValue s currently has
			int tmpSize = getNextSites(links, s, enemy).size();	//tmpSize stands for the amount of neighbors s has
			if(s == agentMove || s == enemy){		//If s is either agentMove or the enemy ignore this Site since they are illegal to move to
			
			}else if(theSite == null){		//If theSite hasn't be set before
				theSite = s;
				csize = temp;
			}else if(csize < temp && tmpSize > 2){	//If the RadiusValue is better and it has at least nextSize - 1 neighbors
				theSite = s;
				csize = temp;
			}
		}
		return theSite;
	}

	/**
	 * This function calculates which Sites can be reached from "agentMove", if enemy is a Site "agentMove" can't pass
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agent This Parameter stores the Position from which all reachable Sites are being calculated
	 * @param enemy This Parameter stores the current Position of the Site "agentMove" can't pass
	 * @return This function returns a Collection of Sites which represent all Sites you can reach from the "agentMove" Site
	 */
	public static Collection<Site> getReachable(Collection<SiteSet> links, Site agent, Site enemy) {
		Collection<Site> ars = new ArrayList<>();	// All reachable sites including those reachable in last iteration
		Collection<Site> lrs = new ArrayList<>();	// Reachable Sites from last iteration
		Collection<Site> rs = new ArrayList<>();	// Reachable Sites in this Iteration
		lrs.add(agent);	// Start from agent field
		boolean changed = true;		//changed checks if there are anymore Sites the loop can reach true if so, false if not
		while (changed) {
			changed = false;
			for (Site a : lrs) {	//This loop will get all Sites which are next to the current Sites in lrs while removing any Sites already visited, which are represented in ars		
				rs.addAll(getNextSites(links, a, enemy));
				rs.removeAll(ars);
			}
			for (Site s : rs) {	//This loop will add the found sites to ars and set changed to true since we found new Sites
				if(!ars.contains(s)){	//A check to be assured that every element is only added once
					ars.add(s);
					changed = true;
				}
			}
			lrs.clear();		//replace lrs with the current contens of rs
			lrs.addAll(rs);
			rs.clear();
		}
		ars.remove(agent);	//remove agent since the Site agent shouldn't be considered in most calculations that use this function
		return ars;
	}

	/**
	 * This function adds up all neighbors of the given Site agent whilst ignoring the enemy Site and returning the found Sites as a Collection 
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agent This Parameter stores the Position of the Site which neighbor Sites are being calculated
	 * @param enemy This Parameter stores the Position of a Site which is to be ignored from the neighbors
	 * @return This function returns a Collection of Sites which represent the Neighbors of "agent" without the enemy Site
	 */
	protected static Collection<Site> getNextSites(Collection<SiteSet> links, Site agent, Site enemy) {
		Collection<Site> rs = new ArrayList<>();	//rs represnts all the sites that are neighboring agent and aren't the enemy
		for (SiteSet s : links) {	//Go through all existing links to get the neighbors of agent
			if(s.getFirst().equals(agent) && s.getSecond() != enemy){	//Check if the frontal or backend Site is the agent and
											// if the neighboring Site is not the enemy
				rs.add(s.getSecond());					//If these conditions apply add the Site that is not agent as a neighbor to the collection
			}else if(s.getSecond().equals(agent) && s.getFirst() != enemy) {
				rs.add(s.getFirst());
			}
		}
		return rs;
	}
	
	/**
	 * @param links This Parameter stores all remaining links of the board
	 * @param agent This Parameter stores the Position where the Player will be moving to
	 * @param enemy This Parameter stores the current Position of the enemy Player
	 * @return This function returns the SiteSet/Link between two Sites that is most optimal for this Computer to remove
	 */
	protected static Collection<SiteSet> getReachableLinks(Collection<SiteSet> links, Site agent, Site enemy){
		Collection<SiteSet> arl = new ArrayList<>();	//arl represents all reachable links from "agent"
		Collection<Site> sfl = new ArrayList<>();	//sfl represents all Sites that are being used for Links
		Collection<Site> boardToSearch = getReachable(links, agent, enemy);	//boardToSearch stands for all Sites out agent can reach
		boardToSearch.add(agent);		//Since getReachable excludes agent we need to add him back since we aren't interested in Sites but SiteSets/links
		
		for(Site s : boardToSearch){		//This loop goes through all Sites that agent can reach,
							// then checks the neighbors and creates a new Siteset/link for each neighbor,
							// afterwards it adds all these new SiteSets/links to arl, if there aren't already present
			sfl = getNextSites(links, s, s);
			for(Site g : sfl){
				SiteSet tmp = new SiteSet(s, g);
				if(!(arl.contains(tmp))){
					arl.add(tmp);
				}
			}
		
		}
		return arl;
	}
    	
	/**
	 * This function calculates a average on how good the amount of movement freedom is for a certain Site
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param fromHere This Parameter stores the Site from which the Radius is expanding
	 * @param enemy This Parameter stores the current Position of the enemy Player
	 * @return This function returns the SiteSet/Link between two Sites that is most optimal for this Computer to remove
	 */
	protected static double getRadiusValue(Collection<SiteSet> links, Site fromHere, Site enemy){
		Collection<Site> ars = new ArrayList<>();	// All reachable sites including those reachable in last iteration
		Collection<Site> lrs = new ArrayList<>();	// Reachable Sites from last iteration
		Collection<Site> rs = new ArrayList<>();	// Reachable Sites in this Iteration
		lrs.add(fromHere);					//Add fromHere to ars so that this iteration can't go through the starting point
		boolean changed = true;					//changed checks if there are anymore Sites the loop can reach true if so, false if not
		double currentValue = 0;				//currentValue represents the current "RadiusValue" for the current branch g
		double currentIteration = 0;				//currentIteration represents the current Iteation the while loop is in
		ars = getNextSites(links, enemy, fromHere);		//We add all neighbor Sites from the enemy to ars
		ars.removeAll(getNextSites(links, enemy, fromHere));	//then remove all Sites that can be reached fromHere,
									// this makes it so that the enemy Site can be included in this calculation but won't go any further
		while(changed) {			//This loop goes through all reachable Sites from the current Site g
			changed = false;
			long tmp = 0;			//tmp represents how many Sites we can reach in this iteration
			for(Site a : lrs){		//This loop will get all Sites which are next to the current Sites in lrs
							// while removing any Sites already visited, which are represented in ars
				rs.addAll(getNextSites(links, a, a));
				rs.removeAll(ars);
			}
			for(Site s : rs) {		//This loop will add the found sites to ars, set changed to true
							// since we found new Sites and count tmp up for every Site found
				if(!ars.contains(s)){	//A check to be assured that every element is only added once
					ars.add(s);
					changed = true;
					tmp = tmp + 1;
				}
			}
			currentValue = currentValue + tmp / (Math.pow(2, currentIteration));	//Afterwards we increase the currentValue,
												// currentIteration while dividing by the current iteration
			currentIteration = currentIteration + 0.5;
			lrs.clear();		//We set lrs to the new Sites found and clear rs
			lrs.addAll(rs);
			rs.clear();
		}
		return currentValue;	//return the sum of all currentValues from all branches in finalValue
	}

	/**
	 * This function checks if the Player can be incapititated
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agent This Parameter stores the current Position of the Player
	 * @param enemy This Parameter stores the current Position of the enemy Player
	 * @return This function returns boolean which is true if a KillMove is possible and false if not
	 */
	protected static boolean isKillMoveAGo(Collection<SiteSet> links, Site agent, Site enemy){
		Collection<Site> toCheck = getNextSites(links, enemy, enemy);	//toCheck represents how many Sites are around the enemy Player
		Collection<Site> reachable = getReachable(links, agent, enemy);	//reachable represents which Sites are reachable by the computer 
		if(Collections.disjoint(toCheck, reachable)){			//This checks if the computer reach the enemy Player's surroundings
			return false;
		}
		if(toCheck.size() == 2 || toCheck.size() == 1){			//This checks if the enemy Player has only one or two more links that sprout
										// from his current Site (Since the KillMove can only be called when the enemy
										// has only one or two links that initiate from the enemies Site and
										// if the computer can reach them)
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * This function tries calculates a Move that will possibly shrink the enemies movements significantly
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agent This Parameter stores the current Position of the computer
	 * @param enemy This Parameter stores the current Position of the enemy Player
	 * @return This function returns a Move object that will either be null (no possibility found) or a certain Move that will reduce the enemies Movement capabilities (possibilities found)
	 */
	protected static Move KillierMove(Collection<SiteSet> links, Site agent, Site enemy){
		Collection<Site> enemyReachable = getReachable(links, enemy, enemy);	//enemyReachable represents the Sites that are reachable by the enemy Player
		Collection<Site> reachable = getReachable(links, agent, enemy);		//reachable represents the Sites reachable by the computer
		
		Move blocking  = null;			//blocking represents the Move that can will block the enemies movements if it isn't null;
		Site toBeBlocked1 = null;		//toBeBlocked 1 represents the Site that will be blocked
		SiteSet toBeBlocked2 = null;		//toBeBlocked 2 represents the SiteSet/link that will be blocked
		int currentBlockedSize = 0;		//currentBlockedSize represents the amount of how many Sites can be blocked by the current toBeBlocked1 and toBeBlocked2
		double currentRadiusSize = 0;		//curent Radius for the Site to move to
		Collection<SiteSet> copyLinks = new ArrayList<>();	//create a temporary link Collection to mimic a removed link
		copyLinks.addAll(links);
		if(Collections.disjoint(enemyReachable, reachable)){
			Site s = getBestSite(links, agent, enemy);
			for(Site g : enemyReachable){
					Collection<Site> fCheck = getNextSites(links, g, g);	
					for(Site f : fCheck){
						copyLinks.remove(new SiteSet(f, g));	//Here we remove the link and then check how many Site the enemy can reach now
						int tmp1 = getReachable(copyLinks, enemy, s).size();	//Depending on that and if currentBlockedSize has been set and
													// if that the amount of blocked sites is at least half of the enemies 
													// reachabel Sites we take the Site and SiteSet/links and
													// save them temporarly as our not yet put together "Move" 
						if(tmp1 < currentBlockedSize || currentBlockedSize == 0 && tmp1 < getReachable(links, enemy, s).size()/2){
							toBeBlocked1 = s;
							toBeBlocked2 = new SiteSet(f, g);
							currentBlockedSize = tmp1;
						}
						copyLinks.clear();
						copyLinks.addAll(links);
					}	
			}
		}else{
			Collection<Site> ars = new ArrayList<>();	// All reachable sites including those reachable in last iteration
			Collection<Site> lrs = new ArrayList<>();	// Reachable Sites from last iteration
			Collection<Site> rs = new ArrayList<>();	// Reachable Sites in this Iteration
			int curArs = 0;				//curIter stands for the current smallest amount of all reachable Sites found
			for(Site s : reachable){		//Take one Site from the computers allowed movements and one from the enemies reachable radius
				for(Site g : enemyReachable){
					Collection<Site> fCheck = getNextSites(links, g, g);	
					for(Site f : fCheck){
						copyLinks.remove(new SiteSet(f, g));	//Here we remove the link and then check how many Site the enemy can reach now
						ars.clear();		//Then execute a modified version of getReachable that disables the Sites s and g from access
						ars.add(s);;
						lrs.add(enemy);			// Start from agent field
						boolean changed = true;		//changed checks if there are anymore Sites the loop can reach true if so, false if not
						while (changed) {
							changed = false;
							for(Site a : lrs) {	//This loop will get all Sites which are next to the current Sites in lrs
										// while removing any Sites already visited, which are represented in ars		
								rs.addAll(getNextSites(links, a, enemy));
								rs.removeAll(ars);
							}
							for(Site h : rs) {	//This loop will add the found sites to ars and set changed to true since we found new Sites
								if(!ars.contains(h)){	//A check to be assured that every element is only added once
									ars.add(h);
									changed = true;
								}
							}
							lrs.clear();		//replace lrs with the current contens of rs
							lrs.addAll(rs);
							rs.clear();
						}
						lrs.clear();
						int tmp0 = enemyReachable.size()/2;		//tmp0 is the smallest amount of Sites that should be blocked
						if(ars.size() < curArs || curArs == 0 && tmp0 >= ars.size() && f != s && g != s){//If the ars Collection in this run
																 // is smaller than in the number saved in curIter
																 // or if the current ars value hasn't been set
																 // and is smaller/equal than the minimum Size
																 // tmp0 
							int tmp1 = getNextSites(links, s, enemy).size();	//tmp1 represents the amount of Sites that are next to s and
														// aren't in the enemies reach if the Move were to be excuted
							for(Site h : getNextSites(links, s, enemy)){
								if(ars.contains(h)){
									tmp1--;
								}
							}
							if(tmp1 > 1){		//If the Site s applies to the requirements we save the current run
								toBeBlocked1 = s;
								toBeBlocked2 = new SiteSet(f, g);
								curArs = ars.size();
							}
						}
						copyLinks.clear();
						copyLinks.addAll(links);
					}
				}
			}
		}
		if(toBeBlocked1 != null && toBeBlocked2 != null){	//If a Site and SiteSet/link have been found create a new Move Object
			blocking = new Move(new SiteTuple(agent, toBeBlocked1), toBeBlocked2);
		}
		return blocking;
	}
	
	/**
	 * This function calculates a move that will hinder all movements the enemy player
	 *
	 * @param links This Parameter stores all remaining links of the board
	 * @param agent This Parameter stores the current Position of the computer
	 * @param enemy This Parameter stores the current Position of the enemy Player
	 * @return This function returns a Move object that will hinder all movements of the enemy
	 */
	protected static Move KillMove(Collection<SiteSet> links, Site agent, Site enemy){
		Collection<Site> toCheck = getNextSites(links, enemy, enemy);	//toCheck represents how many Sites are around the enemy Player	
		Collection<Site> reachable = getReachable(links, agent, enemy);	//reachable represents the Sites reachable by the computer
		Site agentMove = null;		//agentMove is the Site the computer will move to
		SiteSet toRemove = null;	//toRemove is the SiteSet/link the computer will remove
		int size = 0;							//size stands for a new calculated amount of Sites the enemy can reach
		for(Site s : toCheck){					//A for-loop in which agentMove will be set to a Site that the computer can move to and
									// toRemove will be set to a link that isn't the link between the player and
									// enemy to try to block both of the enemies remaining possibilities to move
			if(reachable.contains(s) && agentMove == null && s != agent){
				agentMove = s;
			}else if(toRemove == null){
				toRemove = new SiteSet(s, enemy);
			}
		}
		if(toRemove == null){							//Since there is a possibility that toCheck only contains one Site
											// we need to check if toRemove has been set
			toRemove = new SiteSet(agentMove, enemy);			//If so than take the SiteSet/Link between the computer and enemy
		}
		return new Move(new SiteTuple(agent, agentMove), toRemove);
	}
}
