package nowhere2gopp.graphics;

import nowhere2gopp.preset.*;

public interface VisualUpdater{
	void update(Viewer board);
	
}
