package nowhere2gopp.board;

import java.util.Collection;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;

/**
 * NodeAnalytics class represents a collection of data for a specific field
 * mainly used for the HexagonalBoard.Reachable function
 */
public class NodeAnalytics {
	/**
	 * Reachable links
	 */
	public final Collection<SiteSet> reachableLinks;
	/**
	 * Reachable sites wo agent site
	 */
	public final Collection<Site> allReachableSites;
	/**
	 * preLast iteration reachable sites
	 */
	public final Collection<Site> preLastReachableSites;
	/**
	 * Last iteration reachable sites
	 */
	public final Collection<Site> lastReachableSites;
	/**
	 * Weight of own-position representing a score for the Site
	 * The higher the value the safer
	 */
	public final int weight;
	
	/**
	 * Constructor setting all values
	 * @param rl reachableLinks
	 * @param ars allReachableLinks
	 * @param lrs lastReachableLinks
	 * @param rs reachableSites
	 * @param w weight
	 */
	public NodeAnalytics(Collection<SiteSet> rl,Collection<Site> ars,Collection<Site> lrs,Collection<Site> rs,int w){
		reachableLinks = rl;
		allReachableSites = ars;
		preLastReachableSites = lrs;
		lastReachableSites = rs;
		weight = w;
	}
}
