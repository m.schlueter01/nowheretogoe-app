package nowhere2gopp.board;

import java.util.ArrayList;
import java.util.Collection;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveType;
import nowhere2gopp.preset.Playable;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.Status;
import nowhere2gopp.preset.Viewer;

/**
 * Class for holding and simulating an gameboard of the game NowhereToGo
 */
public class HexagonalBoard implements Viewer, Playable {

	/**
	 * Enumeration holding the 3 gamephases of NowhereToGo LinkLink = first =
	 * two links are removed by each player SetAgent = second = the agent sets
	 * himself on the Place he wants to go AgentLink = last = the agent is moved
	 * and a link is removed
	 */
	public enum GamePhase {
		LinkLink, SetAgent, AgentLink
	};

	/**
	 * Field storing the Sites by its coordinates board[x][y] represents the
	 * Site on (x,y)
	 */
	private final Site[][] board;
	/**
	 * The collection of links as a tuple of Sites
	 */
	protected final Collection<SiteSet> links;
	/**
	 * The size of the field as die diagonal of it Its the logest straight going
	 * connection of Sites
	 */
	private final int size;
	/**
	 * The Site of the red agent
	 */
	protected Site agentRed;
	/**
	 * The Site of the blue agent
	 */
	protected Site agentBlue;
	/**
	 * The state of the Board It detects a win and illegal moves
	 */
	protected Status state;
	/**
	 * The player who's turn it is
	 */
	protected PlayerColor plTurn;
	/**
	 * The count of half rounds made by players A full round contains of two
	 * halfroud or two moves
	 */
	protected int roundHalf;

	/**
	 * Constructor for creating the Board It checks the size and creates a board
	 * starting with (0,0) at bottom right
	 *
	 * @param size longest diagonal through field
	 */
	public HexagonalBoard(int size) {
		// Size limited as specified to odd numbers between 3 and 11
		if (size % 2 != 1 || size > 11 || size < 3) {
			state = Status.Illegal;
			throw new IllegalArgumentException("Illegal size: " + size);
		}
		this.size = size;
		board = new Site[size][size];
		links = new ArrayList<>();

		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board[x].length; y++) {

				// only generate Sites in Hexagonal
				// other Sites stay null
				// upper left and lower right bounds limited
				if (Math.abs(x - y) <= (size / 2)) {
					// first is Column=x second is Row=y
					board[x][y] = new Site(x, y);

					// generate the left lower and lower-left SiteLinks
					// upper left corner restriction of left SiteLink
					if ((y - x) < (size / 2) && x > 0) {
						links.add(new SiteSet(board[x][y], board[x - 1][y]));
					}

					// lower right corner restriction of lower SiteLink
					if ((x - y) < (size / 2) && y > 0) {
						links.add(new SiteSet(board[x][y], board[x][y - 1]));
					}

					// (0,0) Site restriction of lower-left SiteLink
					if (x > 0 && y > 0) {
						links.add(new SiteSet(board[x][y], board[x - 1][y - 1]));
					}
				}
			}
		}

		// Set some default positions
		agentRed = board[size / 2][0];
		agentBlue = board[size / 2][size - 1];

		// Player red always starts
		plTurn = PlayerColor.Red;

		state = Status.Ok;

		// Start with Round # 1 =^= halfRound = 2
		roundHalf = 2;
	}

	/**
	 * Copy constructor generating a board on basis of b2
	 *
	 * @param b2 other board to be copyed
	 */
	public HexagonalBoard(HexagonalBoard b2) {
		this.size = b2.size;
		board = new Site[size][size];
		links = new ArrayList<>();
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board[x].length; y++) {
				if (b2.board[x][y] != null) {
					board[x][y] = new Site(x, y);
				}
			}
		}
		for (SiteSet s : b2.links) {
			links.add(new SiteSet(board[s.getFirst().getColumn()][s.getFirst().getRow()], board[s.getSecond().getColumn()][s.getSecond().getRow()]));
		}
		agentRed = board[b2.agentRed.getColumn()][b2.agentRed.getRow()];
		agentBlue = board[b2.agentBlue.getColumn()][b2.agentBlue.getRow()];
		state = b2.state;
		plTurn = b2.plTurn;
		roundHalf = b2.roundHalf;
	}

	/**
	 * Easteregg
	 *
	 * @return number of places
	 */
	//EASTEREGG
	public int getSeurfac() {
		return this.getNumberOfPlaces();
	}

	/**
	 * returns the number of Sites that are possible in an board of this size
	 *
	 * @return number of sites
	 */
	@Override
	public int getNumberOfPlaces() {
		/*
		IMPORTANT INFORMATION ABOUT THE SIZE OF A HEXAGONAL BOARD:
		1 -> 1; 2 -> 7; 3 -> 19; 4 -> 37; 5 -> 61
		sum = 3*n*n-3*n+1 = 3*n*(n-1)+1
		(proof it with "Gausssche Summenformel" with 2*sum(n, 2*n-2)+2*n-1)
		 */
		return 3 * (this.size / 2 + 1) * ((this.size / 2 + 1) - 1) + 1;
	}

	/**
	 * Returns the Player (its color) which should do the next move
	 *
	 * @return next player move
	 */
	@Override
	public PlayerColor getTurn() {
		return plTurn;
	}

	/**
	 * The diagonal size of the board is returned
	 *
	 * @return diagonal size of board
	 */
	@Override
	public int getSize() {
		return this.size;
	}

	/**
	 * The state of the board is returnd
	 *
	 * @return state of board
	 */
	@Override
	public Status getStatus() {
		return state;
	}

	/**
	 * returns the Site of the Player specified by its color
	 *
	 * @param color agent
	 * @return Site of the Player (color)
	 */
	@Override
	public Site getAgent(PlayerColor color) {
		switch (color) {
			case Red:
				return agentRed;
			case Blue:
				return agentBlue;
		}
		state = Status.Illegal;
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * Returns the list of links as tuple between two Sites
	 *
	 * @return List of Links
	 */
	@Override
	public Collection<SiteSet> getLinks() {
		return this.links;
	}

	/**
	 * Returns the round the next move is done in It starts with 1
	 *
	 * @return round next player moves in
	 */
	public int getRound() {
		return roundHalf / 2;
	}

	/**
	 * Returns the phase of the 3 Gamephases specified
	 *
	 * @return the GamePhase next move is done in
	 */
	public GamePhase getPhase() {
		// the round where the second phase is done
		int seperatorRound = (int) (Math.pow(2, size / 2) + 1);
		if (getRound() < seperatorRound) {
			return GamePhase.LinkLink;
		} else if (getRound() == seperatorRound) {
			return GamePhase.SetAgent;
		} else {
			return GamePhase.AgentLink;
		}
	}

	/**
	 * Creates a new wrapperclass for board
	 *
	 * @return wrapperclass
	 */
	@Override
	public Viewer viewer() {
		return new BoardInformation(this);
	}

	/**
	 * Executes the specified move
	 *
	 * @param move move to be done
	 * @throws IllegalStateException when tryed to move in illegal state
	 */
	public void make(Move move) throws IllegalStateException {

		// -- Check if Status is valid
		// Only valid states are Ok BlueWin RedWin		
		if (state != Status.Ok) {
			throw new IllegalStateException("Tryed to make move in state: " + state);
		}

		// --
		// Excecute moves seperated by type
		if (move.getType() == MoveType.AgentLink) {

			// AgentLink moves an Agent and removes a Link
			// Check if Phase is right
			if (getPhase() != GamePhase.AgentLink && getPhase() != GamePhase.SetAgent) {
				state = Status.Illegal;
				throw new IllegalStateException("Game is in phase " + getPhase() + " but MoveType is" + move.getType());
			}

			// Check if player to be moved has its turn
			switch (getTurn()) {
				case Red:
					if (!getAgent(PlayerColor.Red).equals(move.getAgent().getFirst())) {
						state = Status.Illegal;
						throw new IllegalStateException("Red Player Move but Agent is at " + getAgent(PlayerColor.Red) + " but tryed to move from " + move.getAgent().getFirst());
					}
					break;
				case Blue:
					if (!getAgent(PlayerColor.Blue).equals(move.getAgent().getFirst())) {
						state = Status.Illegal;
						throw new IllegalStateException("Blue Player Move but Agent is at " + getAgent(PlayerColor.Blue) + " but tryed to move from " + move.getAgent().getFirst());
					}
					break;
			}

			PlayerColor c_agent = getTurn();
			PlayerColor c_enemy = getNextPl(c_agent);
			Site s_agent = getAgent(c_agent);
			Site s_enemy = getAgent(c_enemy);

			// Calculate reachable Sites (rs)
			Collection<Site> rs;

			if (getPhase() == GamePhase.SetAgent) {

				// if in special phase set agent the Player can move anywhere
				rs = new ArrayList<>();
				for (int x = 0; x < board.length; x++) {
					for (int y = 0; y < board[x].length; y++) {
						if (Math.abs(x - y) <= (size / 2)) {
							rs.add(board[x][y]);
						}
					}
				}

			} else {
				// Calculate reachable Sites for Agent
				rs = getReachable(links, s_agent, s_enemy).allReachableSites;
				if (move.getAgent().getFirst().equals(move.getAgent().getSecond())) {
					state = Status.Illegal;
					throw new IllegalStateException("tried to move to same field ( no move )");
				}
			}

			// if agent cant move, the game is over ( agentsite is in reachableSites ) 
			if (rs.isEmpty()) {
				if (getTurn() == PlayerColor.Blue) {
					state = Status.RedWin;
				} else if (getTurn() == PlayerColor.Red) {
					state = Status.BlueWin;
				}
				return;
			}

			// Check if the site to move to is reachable from players position
			Site destSite = null;
			for (Site s : rs) {
				// The First is the original Position Second is new Pos
				if (s.equals(move.getAgent().getSecond())) {
					destSite = s;
				}
			}

			if (destSite == null) {
				state = Status.Illegal;
				throw new IllegalStateException("Agent destination field " + move.getAgent().getSecond() + " is not reachable");
			}

			// move the agent who has its turn
			if (move.getAgent().getFirst().equals(agentRed) && plTurn == PlayerColor.Red) {
				agentRed = destSite;
			} else if (move.getAgent().getFirst().equals(agentBlue) && plTurn == PlayerColor.Blue) {
				agentBlue = destSite;
			} else {
				state = Status.Illegal;
				throw new IllegalStateException("Agent (" + plTurn + ") could not be found on " + move.getAgent().getFirst());
			}

			if (move.getLink() == null) {
				state = Status.Illegal;
				throw new NullPointerException("Link is null of Move:" + move);
			}

			// remove the link
			if (!links.remove(move.getLink())) {
				state = Status.Illegal;
				throw new IllegalStateException("Link to remove not found");
			}
			
			// check if enemy lost game
			if(HexagonalBoard.getReachable(links,s_enemy,destSite).allReachableSites.isEmpty()){
				if(c_agent == PlayerColor.Blue){
					state = Status.BlueWin;
				}else{
					state = Status.RedWin;
				}
			}

			// First phase moves			
		} else if (move.getType() == MoveType.LinkLink) {

			// Check right phase
			if (getPhase() != GamePhase.LinkLink) {
				state = Status.Illegal;
				throw new IllegalStateException("Game is in phase " + getPhase() + " but MoveType is" + move.getType());
			}

			// Check if links can be found
			if (!links.remove(move.getOneLink())) {
				state = Status.Illegal;
				throw new IllegalStateException("First link to remove not found + " + move.getOneLink());
			}
			if (!links.remove(move.getOtherLink())) {
				state = Status.Illegal;
				throw new IllegalStateException("Second link to remove not found + " + move.getOtherLink());
			}

		} else if (move.getType() == MoveType.Surrender) {

			// set the winning player
			if (plTurn == PlayerColor.Blue) {
				state = Status.RedWin;
			} else {
				state = Status.BlueWin;
			}

		} else if (move.getType() == MoveType.End) {

			// Check phase. Only in AgentLink is an End possible
			if (getPhase() != GamePhase.AgentLink) {
				state = Status.Illegal;
				throw new IllegalStateException("Game is in phase " + getPhase() + " but MoveType is End");
			}

			// Check if there are sites reachable for the enemy. A move has to be done if possible.
			if (!getReachable(links, getAgent(getTurn()), getAgent(getNextPl(getTurn()))).allReachableSites.isEmpty()) {
				state = Status.Illegal;
				throw new IllegalStateException("Reachable sites but move is type End");
			}

			// set the winning player
			if (plTurn == PlayerColor.Blue) {
				state = Status.RedWin;
			} else {
				state = Status.BlueWin;
			}
		}

		// Set next players turn
		plTurn = getNextPl(plTurn);

		// increase the Round counter
		roundHalf++;
	}

	public static final int MAX_STEP_WIDTH = 10000;

	/**
	 * Calculates all Reachable Sites for an Agent from this Site The Dijksta
	 * Algorithem is used
	 *
	 *
	 * @param links link list
	 * @param agent Site from the Agent to calculate
	 * @param enemy enemy site which blocks moves over this site (can be null if not needed)
	 * @return a list of reachable Sites
	 */
	public static NodeAnalytics getReachable(Collection<SiteSet> links, Site agent, Site enemy) {
		// Use arrays from last iteration to reduce garbageCollector work
		// All reachable sites including those reachable in last iteration
		Collection<Site> ars;
		// Reachable Sites from last iteration
		Collection<Site> lrs;
		// Reachable Sites in this Iteration
		Collection<Site> rs;
		// Reachable Links from agent
		Collection<SiteSet> rl;

		ars = new ArrayList<>();
		lrs = new ArrayList<>();
		rs = new ArrayList<>();
		rl = new ArrayList<>();

		// Start from agent field
		lrs.add(agent);
		boolean changed = true;
		int stepWidth = 0;

		int weight = 0;
		// if the sites in this round were already found there is nothing to do
		while (changed) {
			changed = false;
			// get all reachable sites from last iteration
			stepWidth++;
			for (Site a : lrs) {
				for (SiteSet s : links) {
					if (enemy != null && (s.getFirst().equals(enemy) || s.getSecond().equals(enemy))) {

					} else if (s.getFirst().equals(a)) {
						if (!rs.contains(s.getSecond())) {
							rs.add(s.getSecond());
						}
						if (!rl.contains(s)) {
							rl.add(s);
						}
					} else if (s.getSecond().equals(a)) {
						if (!rs.contains(s.getFirst())) {
							rs.add(s.getFirst());
						}
						if (!rl.contains(s)) {
							rl.add(s);
						}
					}
				}
			}
			rs.removeAll(ars);

			weight += rs.size() * MAX_STEP_WIDTH / stepWidth;

			if (!rs.isEmpty()) {
				changed = true;
			}

			ars.addAll(rs);

			lrs.clear();
			lrs.addAll(rs);
			rs.clear();
		}
		ars.remove(agent);
		return new NodeAnalytics(rl, ars, lrs, rs, weight);
	}

	/**
	 * Returns the Sites reachable with one step from Site agent with Links
	 * links
	 *
	 * @param links links possible for agent
	 * @param agent agent to calculate site from
	 * @return a list of close next site (radius 1)
	 */
	public static Collection<Site> getNextSites(Collection<SiteSet> links, Site agent) {
		Collection<Site> rs = new ArrayList<>();
		for (SiteSet s : links) {
			// check wher agent is and add other site
			if (s.getFirst().equals(agent)) {
				rs.add(s.getSecond());
			} else if (s.getSecond().equals(agent)) {
				rs.add(s.getFirst());
			}
		}
		return rs;
	}

	/**
	 * Removes all links to a Site from a given link-list
	 *
	 * @param links list of links to remove site from
	 * @param toRemove site to remove
	 */
	public static void removeSite(Collection<SiteSet> links, Site toRemove) {
		Collection<SiteSet> temp = new ArrayList<>();
		for (SiteSet s : links) {
			// add links to remove to a temporary list and remove afterwards
			if (s.getFirst().equals(toRemove)) {
				temp.add(s);
			} else if (s.getSecond().equals(toRemove)) {
				temp.add(s);
			}
		}
		links.removeAll(temp);
	}

	/**
	 * Calculates the next player with turn
	 * @param actPl the last player
	 * @return next player
	 */
	public static PlayerColor getNextPl(PlayerColor actPl) {
		return (actPl == PlayerColor.Blue ? PlayerColor.Red : PlayerColor.Blue);
	}
}
