package nowhere2gopp.player;

import java.util.ArrayList;
import java.util.Collection;
import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.board.HexagonalBoard.GamePhase;
import nowhere2gopp.board.NodeAnalytics;
import nowhere2gopp.base.NowhereToGoe;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveType;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.SiteTuple;

/**
 * Basic Strategic ComputerPlayer on basis of Project specification
 */
public class StrategicComputerPlayer extends BasePlayer {

	/**
	 * Calculates the Strategic Move
	 *
	 * @return calcluated move
	 * @throws Exception if in illegal state for request (call sequence)
	 */
	@Override
	public Move request() throws Exception {
		if (pfcs != PlFuncCallState.request) {
			throw new IllegalStateException("Tried call request before update");
		}

		Move next = new Move(MoveType.End);

		if (board.getPhase() == GamePhase.LinkLink) {
			SiteSet linkToRemove = null;

			while (linkToRemove == null) {
				for (SiteSet s : board.getLinks()) {
					if (Math.random() > 0.99) {
						linkToRemove = s;
					}
				}
			}

			SiteSet link2ToRemove = null;

			while (link2ToRemove == null) {
				for (SiteSet s : board.getLinks()) {
					if (Math.random() > 0.99 && s != linkToRemove) {
						link2ToRemove = s;
					}
				}
			}

			next = new Move(linkToRemove, link2ToRemove);
		} else if (board.getPhase() == GamePhase.SetAgent) {
			int max = 0;
			Site newAgentSite = null;
			for (SiteSet s : board.getLinks()) {
				int n_links = HexagonalBoard.getReachable(board.getLinks(), s.getFirst(), board.getAgent(oppColor)).weight;
				if (max < n_links) {
					max = n_links;
					newAgentSite = s.getFirst();
				}
			}
			SiteSet linkToRemove = null;

			while (linkToRemove == null) {
				for (SiteSet s : board.getLinks()) {
					if (Math.random() > 0.99) {
						linkToRemove = s;
					}
				}
			}

			next = new Move(new SiteTuple(board.getAgent(color), newAgentSite), linkToRemove);
		} else {
			// -- Agent Link Phase --
			
			// Get analytics
			NodeAnalytics agentAnalytics = HexagonalBoard.getReachable(board.getLinks(), board.getAgent(color), board.getAgent(oppColor));
			Site newAgentSite = null;
			SiteSet bestEnemyLink = null;
			
			// check if lost
			if (agentAnalytics.allReachableSites.isEmpty()) {
				next = new Move(MoveType.End);
			} else {
				// if not lost calculate move
				newAgentSite = getBiggestNode(agentAnalytics.allReachableSites, board.getLinks(), board.getAgent(color), board.getAgent(oppColor));
				bestEnemyLink = getBiggestLink(board.getLinks(), board.getAgent(oppColor), board.getAgent(color));
			}
			
			// only create move if evry calculation succeded
			if (newAgentSite != null) {
				if (bestEnemyLink != null) {
					next = new Move(new SiteTuple(board.getAgent(color), newAgentSite), bestEnemyLink);
				} else {
					// Random link if no link found
					SiteSet linkToRemove = RandomPlayer.getRandomRemoveLink(board.getLinks(), null);
					next = new Move(new SiteTuple(board.getAgent(color), newAgentSite), linkToRemove);
				}
			}
		}

		if (next.getType() != MoveType.End) {
			System.out.println("sp : " + next);
		}

		board.make(next);

		pfcs = PlFuncCallState.confirm;

		return next;
	}

	/**
	 * returns the best weight node for agent wich is reachable without moving
	 * over enemy site.enemy can be null. then this is ignored. it only returns
	 * a site from starting sites calculates the reachable sites for agent with
	 * links. but links can contain all links.
	 *
	 * @param starting_sites sites to calc biggest site from (only returns one
	 * of those)
	 * @param links existing links
	 * @param agent agents site
	 * @param enemy enemy site
	 * @return highest weight site
	 */
	public static Site getBiggestNode(Collection<Site> starting_sites, Collection<SiteSet> links, Site agent, Site enemy) {
		if (starting_sites.isEmpty()) {
			throw new IllegalArgumentException("start sites are empty");
		}
		if (links.isEmpty()) {
			throw new IllegalStateException("no link is there");
		}
		if (agent.equals(enemy)) {
			throw new IllegalArgumentException("agent is enemy");
		}

		Site max = null;

		int max_weight = -1;

		// Search for tree with most move possibilitys
		for (Site s : starting_sites) {
			if (s == agent) {
				continue;
			}
			int size = HexagonalBoard.getReachable(links, s, enemy).weight;
			if (size > max_weight) {
				max = s;
				max_weight = size;
			}
		}

		if (max == null) {
			throw new IllegalStateException("No Reachable Link");
		}

		return max;
	}

	/**
	 * Highest value Link lowering the weight of agent by the highest value
	 *
	 * @param links existing links
	 * @param agent agents site
	 * @param enemy enemy site
	 * @return the most dangerous link for agent
	 */
	public static SiteSet getBiggestLink(Collection<SiteSet> links, Site agent, Site enemy) {
		if (links.isEmpty()) {
			throw new IllegalStateException("no link is there");
		}
		if (agent.equals(enemy)) {
			throw new IllegalArgumentException("agent is enemy");
		}

		NodeAnalytics enemyAna = HexagonalBoard.getReachable(links, agent, enemy);

		Collection<SiteSet> wk_links = new ArrayList<>(enemyAna.reachableLinks);

		SiteSet max = null;

		int lowest_weight = Integer.MAX_VALUE;

		// Search for tree with most move possibilitys
		for (SiteSet l : enemyAna.reachableLinks) {
			wk_links.remove(l);
			int weight = HexagonalBoard.getReachable(wk_links, agent, enemy).weight;
			if (weight < lowest_weight) {
				max = l;
				lowest_weight = weight;
			}
			wk_links.add(l);
		}

		if (max == null) {
			throw new IllegalArgumentException("no reachable site");
		}

		return max;
	}

	/**
	 * Calculates the killmove when enemy has only one link connected to his
	 * site. Must check if it is the case and the agent has any reachable sites.
	 *
	 * @param next_enemy_sites the sites around the enemy with one site distance
	 * @param agent agent
	 * @param reachableDataAgent reachable data agent
	 * @param enemy enemy
	 * @param reachableDataEnemy reachable data enemy
	 * @return the move to make to kill enemy
	 */
	public static Move calculateKillMoveOne(Collection<Site> next_enemy_sites, Site agent, NodeAnalytics reachableDataAgent, Site enemy, NodeAnalytics reachableDataEnemy) {
		if (next_enemy_sites.size() != 1) {
			throw new IllegalArgumentException("cant do this on next sites 1");
		}

		if (reachableDataAgent.allReachableSites.isEmpty()) {
			throw new RuntimeException("cant do this without possible move");
		}

		SiteSet linkToRemove = null;

		for (SiteSet l : reachableDataEnemy.reachableLinks) {
			if (l.getFirst().equals(enemy) || l.getSecond().equals(enemy)) {
				linkToRemove = l;
			}
		}

		if (linkToRemove == null) {
			throw new RuntimeException("link to remove is null");
		}

		Site nextNode = RandomPlayer.getRandomSite(reachableDataAgent.allReachableSites, agent);

		Move rm = new Move(new SiteTuple(agent, nextNode), linkToRemove);

		return rm;
	}

	/**
	 * Calculates the Move to Kill enemy when he has only to links left to his
	 * site. Must check if its only 2 sites before and agent can move Returns
	 * move to kill enemy or null if site is not reachable for agent
	 *
	 * @param next_enemy_sites sites around enemy with one link distance
	 * @param agent agent
	 * @param reachableDataAgent agent reachable data
	 * @param enemy enemy
	 * @param reachableDataEnemy enemy reachable data
	 * @return the move if possible otherwise null
	 */
	public static Move calculateKillMoveTwo(Collection<Site> next_enemy_sites, Site agent, NodeAnalytics reachableDataAgent, Site enemy, NodeAnalytics reachableDataEnemy) {
		if (next_enemy_sites.size() != 2) {
			throw new IllegalArgumentException("cant do this on next sites 2");
		}

		if (reachableDataAgent.allReachableSites.isEmpty()) {
			throw new RuntimeException("cant do this without possible move");
		}

		Site nextNode = null;
		Move rm = null;
		SiteSet linkToRemove = null;
		for (Site s : next_enemy_sites) {
			if (reachableDataAgent.allReachableSites.contains(s)) {
				nextNode = s;
			}
		}

		if (nextNode == null) {
			if (NowhereToGoe.DEBUG) {
				System.out.println("cant do killmove bcs not reachable");
			}
			return null;
		}

		Site otherSite = null;
		for (Site s : next_enemy_sites) {
			if (!s.equals(nextNode)) {
				otherSite = s;
			}
		}

		for (SiteSet l : reachableDataEnemy.reachableLinks) {
			if ((l.getFirst().equals(enemy) && l.getSecond().equals(otherSite))
					|| (l.getSecond().equals(enemy) && l.getFirst().equals(otherSite))) {
				linkToRemove = l;
			}
		}
		if (linkToRemove == null) {
			throw new RuntimeException("link to remove is null nn:" + nextNode + " os:" + otherSite + " rl:" + reachableDataEnemy.reachableLinks);
		}

		rm = new Move(new SiteTuple(agent, nextNode), linkToRemove);

		return rm;
	}
}
