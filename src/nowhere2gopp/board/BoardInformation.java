package nowhere2gopp.board;

import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.preset.*;
import java.util.Collection;

/**
*Class to get information about the hexagonalboard
*@author Nico Hundertmark
*/
public class BoardInformation implements Viewer{
	HexagonalBoard board;

	public BoardInformation(HexagonalBoard board){
		this.board = board;
	}

	public PlayerColor getTurn(){
		return board.getTurn();
	}

	public int getSize(){
		return board.getSize();
	}

	public Status getStatus(){
		return board.getStatus();
	}


	public Site getAgent(final PlayerColor color){
		return board.getAgent(color);
	}


	public Collection<SiteSet> getLinks(){
		return board.getLinks();
	}

	public int getNumberOfPlaces(){
		return board.getNumberOfPlaces();
	}

	@Override
	public HexagonalBoard.GamePhase getPhase() {
		return board.getPhase();
	}
}
