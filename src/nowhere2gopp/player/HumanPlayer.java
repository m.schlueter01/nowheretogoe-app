package nowhere2gopp.player;
import nowhere2gopp.preset.Requestable;
import nowhere2gopp.preset.Move;

/**
 * Playerclass to get moves from human
 * @author marius schlueter
 */
public class HumanPlayer extends BasePlayer{
	/**
	 * Requestable object from where to get the moves
	 */
	Requestable req;
	/**
	 * Constructor to generate this with a given requestable
	 * @param req requestable to get moves from
	 */
	public HumanPlayer(Requestable req){
		if(req == null) {
			throw new IllegalArgumentException("Requestable is null");
		}
		this.req = req;
	}
	/**
	 * Returns the move from the Requestable
	 * @return the move from Requestable
	 * @throws Exception if call sequence not ok and move not valid
	 */
	@Override
	public Move request() throws Exception {
		if (pfcs != PlFuncCallState.request) {
			throw new IllegalStateException("Tried call request before update");
		}

		Move next = null;

		next = req.request();

		board.make(next);

		pfcs = PlFuncCallState.confirm;

		return next;
	}
}
