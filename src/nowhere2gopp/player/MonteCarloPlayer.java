package nowhere2gopp.player;

import nowhere2gopp.player.RandomPlayer;
import nowhere2gopp.player.StrategicComputerPlayer;
import java.rmi.RemoteException;
import java.util.Collection;
import nowhere2gopp.board.HexagonalBoard.GamePhase;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveType;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.Status;
import java.util.ArrayList;
import java.util.Iterator;
import nowhere2gopp.graphics.BasicVisualizer;
import nowhere2gopp.graphics.Graphical;
import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.board.NodeAnalytics;
import nowhere2gopp.base.NowhereToGoe;
import nowhere2gopp.board.ReducedHexagonalBoard;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.SiteTuple;

/**
 * Class for an AI-Player with Mote-Carlo strategie
 *
 * @author marius schlueter
 */
public class MonteCarloPlayer extends BasePlayer implements Runnable {

	/**
	 * Maximum number of moves calculated per simulation
	 */
	private final int PRESIM_RANGE;
	/**
	 * Number of Simulations (Random Game played till end or presimrange)
	 */
	private final int SIMULATIONS;
	/**
	 * Random Moves(Sites)-count for MonteCarlo player to calculate the
	 * statistical chance for
	 */
	private final int POTENTIAL_SITES;
	/**
	 * Scaling factor how to scale the Simulationcount in dependence of
	 * Own-position-weight
	 */
	private final int SIM_SCALING;
	/**
	 * Divisor activating the Randomness of move calculations in dependence of
	 * Own-positon-weight Higher is more randomness
	 */
	private final int RANDOM_ACTIVATION_DIVISOR;
	/**
	 * Count of moves calculated on an more calculative-algorithem when its not
	 * in random-calculation
	 */
	private final int ADVANCED_PRECALC_MOVES;
	/**
	 * Offset of winnig percentage on the Move of StrategicPlayer BestAgentSite
	 * when BestEnemyLink is removed and BestEnemyLink without AgentPosition
	 */
	private final int STP_MOVE_OFFSET;

	/**
	 * Mutex for Syncronization of ThreadStart and the Result calculation at the
	 * end
	 */
	private Object mutex = new Object();
	/**
	 * Move to Calculate the winnig chance for in the next thread
	 */
	private Move moveToCalc;
	/**
	 * The Best move by winnig chance
	 */
	private Move best_move;
	/**
	 * The score (winnig chance) of best_move This value is not in percent but
	 * in the raw score This is the sum of winns Has to be divided by
	 * simulationcount to get percentage
	 */
	private int max_score;
	/**
	 * The thread, this Player is using at the moment
	 */
	private Collection<Thread> worker = new ArrayList<Thread>();
	/**
	 * The score of the StrategicPlayer move BestAgentSite when BestEnemyLink is
	 * removed and BestEnemyLink without AgentPosition
	 */
	private int stp_score;
	/**
	 * Move calculated by StrategicPlayer BestAgentSite when BestEnemyLink is
	 * removed and BestEnemyLink without AgentPosition
	 */
	private Move stp_move;
	/**
	 * Local simulation-count calculated each round by its own-position-weight
	 */
	private int boardSpecificSimulations;

	/**
	 * Datacolletion of own position
	 */
	private NodeAnalytics reachableDataAgent;
	/**
	 * Datacollection of enemy position
	 */
	private NodeAnalytics reachableDataEnemy;

	/**
	 * Defaultconstructor sets the parameter default values
	 */
	public MonteCarloPlayer() {
		PRESIM_RANGE = 10000;
		SIMULATIONS = 1000;
		POTENTIAL_SITES = 0;
		SIM_SCALING = 1;
		RANDOM_ACTIVATION_DIVISOR = 1000;
		ADVANCED_PRECALC_MOVES = 100000;
		STP_MOVE_OFFSET = 0;
	}

	/**
	 * Konstructor with each parameter to set
	 *
	 * @param psr Maximum numbe of moves to calculate
	 * @param sim Basevalue of Simulation taken for each move
	 * @param pos Number of Random Sites to Calculate Moves for
	 * @param ss Scaling value of Simulations in dependency of
	 * own-position-weight
	 * @param rad Randomness factor in calculations
	 * @param apm Number of more calculative moves taken in each Simulation at
	 * first
	 * @param smo Percentage offset of StrategicPlayer Move
	 */
	public MonteCarloPlayer(int psr, int sim, int pos, int ss, int rad, int apm, int smo) {
		PRESIM_RANGE = psr;
		SIMULATIONS = sim;
		POTENTIAL_SITES = pos;
		SIM_SCALING = ss;
		RANDOM_ACTIVATION_DIVISOR = rad;
		ADVANCED_PRECALC_MOVES = apm;
		STP_MOVE_OFFSET = smo;
	}

	/**
	 * Calculate a move Can take some time dependent of Parameters
	 *
	 * @return Best Move calculated by this players algorithem
	 * @throws Exception illegalstate if try move in illegal state
	 * @throws RemoteException some network things
	 */
	@Override
	public Move request() throws Exception, RemoteException {
		if (pfcs != PlFuncCallState.request) {
			throw new IllegalStateException("Tried call request before update");
		}

		if (board.getStatus() != Status.Ok) {
			throw new IllegalArgumentException("Tried to request move in state " + board.getStatus());
		}

		// First two gamephases are basicly random
		if (board.getPhase() == GamePhase.LinkLink) {

			Move next = RandomPlayer.getRandomMove(board, board.getAgent(color), board.getAgent(oppColor));

			board.make(next);

			pfcs = PlFuncCallState.confirm;

			return next;
		} else if (board.getPhase() == GamePhase.SetAgent) {
			int max = 0;
			Site newAgentSite = null;
			for (SiteSet s : board.getLinks()) {
				int n_links = HexagonalBoard.getReachable(board.getLinks(), s.getFirst(), board.getAgent(oppColor)).weight;
				if (max < n_links && !board.getAgent(oppColor).equals(s.getFirst())) {
					max = n_links;
					newAgentSite = s.getFirst();
				}
			}
			SiteSet linkToRemove = null;

			while (linkToRemove == null) {
				for (SiteSet s : board.getLinks()) {
					if (Math.random() > 0.99) {
						linkToRemove = s;
					}
				}
			}

			Move next = new Move(new SiteTuple(board.getAgent(color), newAgentSite), linkToRemove);

			board.make(next);

			pfcs = PlFuncCallState.confirm;

			return next;
		}

		System.out.println("--------------------------------------------");
		System.out.println();
		System.out.println("Player " + color + " turn");

		// Get Context informatein of enemy and agent
		reachableDataAgent = HexagonalBoard.getReachable(board.getLinks(), board.getAgent(color), board.getAgent(oppColor));
		reachableDataEnemy = HexagonalBoard.getReachable(board.getLinks(), board.getAgent(oppColor), board.getAgent(color));

		// Check if lost
		if (reachableDataAgent.allReachableSites.isEmpty()) {
			Move next = new Move(MoveType.End);
			board.make(next);
			pfcs = PlFuncCallState.confirm;
			System.out.println("Carlo lost " + board.getStatus());
			return next;
		}
		// Check if win
		if (reachableDataEnemy.allReachableSites.isEmpty()) {
			Move next = new Move(MoveType.End);
			board.make(next);
			pfcs = PlFuncCallState.confirm;
			System.out.println("Carlo win " + board.getStatus());
			return next;
		}
		// Check if easy win situation
		Collection<Site> next_enemy_sites = HexagonalBoard.getNextSites(board.getLinks(), board.getAgent(oppColor));
		Move mv = null;
		if (next_enemy_sites.size() == 2) {
			mv = StrategicComputerPlayer.calculateKillMoveTwo(next_enemy_sites, board.getAgent(color), reachableDataAgent, board.getAgent(oppColor), reachableDataEnemy);
		} else if (next_enemy_sites.size() == 1) {
			mv = StrategicComputerPlayer.calculateKillMoveOne(next_enemy_sites, board.getAgent(color), reachableDataAgent, board.getAgent(oppColor), reachableDataEnemy);
		}
		if (mv != null) {
			board.make(mv);
			pfcs = PlFuncCallState.confirm;
			System.out.println("Carlo win " + board.getStatus());
			return mv;
		}

		// Prepare Calculation
		boardSpecificSimulations = SIMULATIONS + (SIMULATIONS * SIM_SCALING * HexagonalBoard.MAX_STEP_WIDTH) / (reachableDataAgent.weight);
		best_move = null;
		max_score = 0;

		// Calculate move from Strategic player where Agent sits on his biggest node
		SiteSet bestEnemyLink = StrategicComputerPlayer.getBiggestLink(board.getLinks(), board.getAgent(oppColor), null);
		Collection<SiteSet> links_wo_rm_link = new ArrayList<>(board.getLinks());
		links_wo_rm_link.remove(bestEnemyLink);
		Site newAgentSite = null;
		int best_weight = -1;
		for (Site r : reachableDataAgent.allReachableSites) {
			int weight = HexagonalBoard.getReachable(links_wo_rm_link, r, null).weight;
			if (weight > best_weight && !r.equals(board.getAgent(color))) {
				newAgentSite = r;
				best_weight = weight;
			}
			if (NowhereToGoe.DEBUG) {
				System.out.println("" + r + "\t" + weight);
			}
		}
		moveToCalc = new Move(new SiteTuple(board.getAgent(color), newAgentSite), bestEnemyLink);
		System.out.println("stp move : " + moveToCalc);
		stp_move = moveToCalc;
		Thread t = new Thread(this);
		worker.add(t);
		t.start();

		// Calculate if Agent Sits on the enemy biggest node and bestenemylink is deleted	
		Collection<SiteSet> w_links = new ArrayList<>(board.getLinks());
		w_links.remove(bestEnemyLink);
		Site biggest_node = StrategicComputerPlayer.getBiggestNode(reachableDataAgent.allReachableSites, w_links, board.getAgent(oppColor), null);
		if (biggest_node != null && biggest_node.equals(board.getAgent(color))) {
			biggest_node = null;
			System.out.println("1 biggest enemy node is agent position");
		}
		if (biggest_node != null) {
			waitForThreadStart();
			moveToCalc = new Move(new SiteTuple(board.getAgent(color), biggest_node), bestEnemyLink);
			System.out.println("biggest enemy node + enemy link : " + moveToCalc);
			t = new Thread(this);
			worker.add(t);
			t.start();
		}

		// Biggest Link when Enemy is blocked by agent position
		// Then calculate biggest enemy node
		SiteSet enlwa = StrategicComputerPlayer.getBiggestLink(board.getLinks(), board.getAgent(oppColor), board.getAgent(color));
		w_links.add(bestEnemyLink);
		w_links.remove(enlwa);

		if (NowhereToGoe.DEBUG) {
			BasicVisualizer bvdbg = new BasicVisualizer(w_links);
			bvdbg.update();
			bvdbg.dispose();
		}

		Site biggest_node_2 = StrategicComputerPlayer.getBiggestNode(reachableDataAgent.allReachableSites, w_links, board.getAgent(oppColor), null);
		if (biggest_node_2 != null && biggest_node_2.equals(board.getAgent(color))) {
			throw new RuntimeException("node is agent");
		}
		if (biggest_node_2 != null) {
			waitForThreadStart();
			moveToCalc = new Move(new SiteTuple(board.getAgent(color), biggest_node_2), bestEnemyLink);
			System.out.println("biggest enemy node + enemy link when enemy is blocked by agent : " + moveToCalc);
			t = new Thread(this);
			worker.add(t);
			t.start();
		}

		// Calculate if EnemyLink is blocked by agent and another link is removed
		w_links.add(bestEnemyLink);
		if (!board.getAgent(oppColor).equals(bestEnemyLink.getFirst())) {
			SiteSet secondEnemyLink1 = StrategicComputerPlayer.getBiggestLink(board.getLinks(), board.getAgent(oppColor), bestEnemyLink.getFirst());
			if (reachableDataAgent.allReachableSites.contains(bestEnemyLink.getFirst()) && !bestEnemyLink.getFirst().equals(board.getAgent(color))) {
				waitForThreadStart();
				moveToCalc = new Move(new SiteTuple(board.getAgent(color), bestEnemyLink.getFirst()), secondEnemyLink1);
				System.out.println("sit on best enemy link and remove secondbest 1 : " + moveToCalc);
				t = new Thread(this);
				worker.add(t);
				t.start();
			}
		}
		if (!board.getAgent(oppColor).equals(bestEnemyLink.getSecond())) {
			SiteSet secondEnemyLink2 = StrategicComputerPlayer.getBiggestLink(board.getLinks(), board.getAgent(oppColor), bestEnemyLink.getSecond());
			if (reachableDataAgent.allReachableSites.contains(bestEnemyLink.getSecond()) && !bestEnemyLink.getSecond().equals(board.getAgent(color))) {
				waitForThreadStart();
				moveToCalc = new Move(new SiteTuple(board.getAgent(color), bestEnemyLink.getSecond()), secondEnemyLink2);
				System.out.println("sit on best enemy link and remove secondbest 2 : " + moveToCalc);
				t = new Thread(this);
				worker.add(t);
				t.start();
			}
		}

		// Calculate player sitting around enemy
		Collection<Site> w_sites = HexagonalBoard.getNextSites(reachableDataEnemy.reachableLinks, board.getAgent(oppColor));
		// only if more than two site are there
		if (w_sites.size() >= 2) {
			// get two random sites from enemy
			Iterator<Site> myit = w_sites.iterator();
			Site nextEnemySite = myit.next();
			Site siteToSitAgent = myit.next();

			SiteSet agentLink = null;
			SiteSet removeLink = null;

			for (SiteSet ml : reachableDataEnemy.reachableLinks) {
				if (ml.getFirst().equals(nextEnemySite) && ml.getSecond().equals(board.getAgent(oppColor))
						|| ml.getSecond().equals(nextEnemySite) && ml.getFirst().equals(board.getAgent(oppColor))) {
					removeLink = ml;
				} else if (ml.getFirst().equals(siteToSitAgent) && ml.getSecond().equals(board.getAgent(oppColor))
						|| ml.getSecond().equals(siteToSitAgent) && ml.getFirst().equals(board.getAgent(oppColor))) {
					agentLink = ml;
				}
			}

			waitForThreadStart();
			if (reachableDataAgent.allReachableSites.contains(siteToSitAgent)) {
				moveToCalc = new Move(new SiteTuple(board.getAgent(color), siteToSitAgent), removeLink);
				System.out.println("sit around enemy 1 : " + moveToCalc);
				t = new Thread(this);
				worker.add(t);
				t.start();
			}

			if (reachableDataAgent.allReachableSites.contains(nextEnemySite)) {
				waitForThreadStart();
				moveToCalc = new Move(new SiteTuple(board.getAgent(color), nextEnemySite), agentLink);
				System.out.println("sit around enemy 2 : " + moveToCalc);
				t = new Thread(this);
				worker.add(t);
				t.start();
			}
		}

		if (POTENTIAL_SITES < reachableDataAgent.allReachableSites.size() || boardSpecificSimulations > SIM_SCALING * SIMULATIONS / RANDOM_ACTIVATION_DIVISOR) {
			System.out.println("Random mode");
			// Start threads calculating
			for (int calculated_sites = 0; calculated_sites < POTENTIAL_SITES; calculated_sites++) {
				waitForThreadStart();
				if (boardSpecificSimulations > SIM_SCALING * SIMULATIONS / RANDOM_ACTIVATION_DIVISOR) {
					moveToCalc = RandomPlayer.getRandomMove(board, board.getAgent(color), board.getAgent(oppColor));
				} else {
					moveToCalc = new Move(RandomPlayer.getRandomMove(board, board.getAgent(color), board.getAgent(oppColor)).getAgent(), bestEnemyLink);
				}
				t = new Thread(this);
				worker.add(t);
				t.start();
			}
		} else {
			System.out.println("Normal mode");
			for (Site s : reachableDataAgent.allReachableSites) {
				if (s.equals(board.getAgent(color))) {
					continue;
				}
				waitForThreadStart();
				if (boardSpecificSimulations > SIM_SCALING * SIMULATIONS / RANDOM_ACTIVATION_DIVISOR) {
					moveToCalc = new Move(new SiteTuple(board.getAgent(color), s), RandomPlayer.getRandomRemoveLink(board.getLinks(), reachableDataEnemy.reachableLinks));
				} else {
					moveToCalc = new Move(new SiteTuple(board.getAgent(color), s), bestEnemyLink);
				}
				t = new Thread(this);
				worker.add(t);
				t.start();
			}
		}

		System.out.println("Threads started with " + boardSpecificSimulations + " simulations");

		// Wait for threads to end
		for (Thread ct : worker) {
			ct.join();
		}
		worker.clear();

		if (best_move == null) {
			if (NowhereToGoe.DEBUG) {
				throw new RuntimeException("no best move found");
			}
			best_move = RandomPlayer.getRandomMove(board, board.getAgent(color), board.getAgent(oppColor));
		}

		// Print result
		if (!best_move.equals(stp_move)) {
			System.out.println("Choose random " + best_move + " with score " + max_score / boardSpecificSimulations);
		} else {
			System.out.println("Choose stp_move " + best_move + " with score " + max_score / boardSpecificSimulations);
		}

		if ((max_score / boardSpecificSimulations) < 50) {
			System.out.println("Move with negative score taken " + best_move + "\t" + (max_score / boardSpecificSimulations));
		}

		if ((max_score / boardSpecificSimulations) > 60) {
			System.out.println("Move with great score taken " + best_move + "\t" + (max_score / boardSpecificSimulations));
		}

		// Normal Player operations
		Move next = best_move;
		board.make(next);
		pfcs = PlFuncCallState.confirm;

		return next;
	}

	/**
	 * Wait until an Thread (running this class as Runnable) copyied his move to
	 * calculate.
	 */
	private void waitForThreadStart() {
		while (true) {
			synchronized (mutex) {
				if (moveToCalc == null) {
					break;
				}
			}
			Thread.yield();
		}
	}

	/**
	 * Method to calculate the percentage chance of winning for a specific move
	 */
	@Override
	public void run() {
		Move m = null;
		synchronized (mutex) {
			if (moveToCalc == null) {
				return;
			}
			m = moveToCalc;
			moveToCalc = null;
		}

		HexagonalBoard bsim = null;
		int sco = 0;
		PlayerColor[] sim_pl = new PlayerColor[]{oppColor, color};
		// Starts with opponent bc the first move is done without any calculations
		// its the given move to simulate
		for (int k = 0; k < boardSpecificSimulations; k++) {
			int turn = 0;
			bsim = new ReducedHexagonalBoard(board);
			bsim.make(m);
			for (int i = 0; i < PRESIM_RANGE; i++) {
				Site agent = bsim.getAgent(sim_pl[turn]);
				Site enemy = bsim.getAgent(sim_pl[turn ^ 1]);

				NodeAnalytics sim_reachableDataAgent = HexagonalBoard.getReachable(bsim.getLinks(), agent, enemy);
				NodeAnalytics sim_reachableDataEnemy = HexagonalBoard.getReachable(bsim.getLinks(), enemy, null);

				Move rm = null;

				if (!sim_reachableDataAgent.allReachableSites.isEmpty()) {

					Collection<Site> next_enemy_sites = HexagonalBoard.getNextSites(bsim.getLinks(), enemy);
					Site nextNode = null;
					SiteSet linkToRemove = null;

					if (next_enemy_sites.size() == 2) {
						rm = StrategicComputerPlayer.calculateKillMoveTwo(next_enemy_sites, agent, sim_reachableDataAgent, enemy, sim_reachableDataEnemy);
					} else if (next_enemy_sites.size() == 1) {
						rm = StrategicComputerPlayer.calculateKillMoveOne(next_enemy_sites, agent, sim_reachableDataAgent, enemy, sim_reachableDataEnemy);
					}

					if (rm == null || rm.getType() == MoveType.End) {
						int random = (int) (Math.random() * (sim_reachableDataAgent.allReachableSites.size() - 1));
						Iterator<Site> myits = sim_reachableDataAgent.allReachableSites.iterator();
						nextNode = myits.next();
						for (int rc = 0; rc < random; rc++) {
							Site n = myits.next();
							if (!n.equals(agent)) {
								nextNode = n;
							}
						}

						if (nextNode == null) {
							throw new RuntimeException("node is null");
						}

						if (k < ADVANCED_PRECALC_MOVES && boardSpecificSimulations < SIM_SCALING * SIMULATIONS / RANDOM_ACTIVATION_DIVISOR) {
							linkToRemove = StrategicComputerPlayer.getBiggestLink(bsim.getLinks(), enemy, null);
							Collection<SiteSet> wlinks = new ArrayList<>(bsim.getLinks());
							wlinks.remove(linkToRemove);
							nextNode = StrategicComputerPlayer.getBiggestNode(sim_reachableDataAgent.allReachableSites, wlinks, agent, null);
							if (nextNode == null) {
								throw new RuntimeException("node is null");
							}
							rm = new Move(new SiteTuple(agent, nextNode), linkToRemove);
						} else {
							if (sim_reachableDataAgent.reachableLinks.isEmpty()) {
								linkToRemove = bsim.getLinks().iterator().next();
							} else {
								random = (int) (Math.random() * (sim_reachableDataAgent.reachableLinks.size() - 1));
								Iterator<SiteSet> myitl = sim_reachableDataAgent.reachableLinks.iterator();
								linkToRemove = myitl.next();
								for (int rc = 0; rc < random; rc++) {
									linkToRemove = myitl.next();
								}
							}
							rm = new Move(new SiteTuple(agent, nextNode), linkToRemove);
						}
					}

				} else {
					rm = new Move(MoveType.End);
				}

				bsim.make(rm);
				if (bsim.getStatus() != Status.Ok) {
					break;
				}
				turn = turn ^ 1;
			}
			sco += getScoreAgent(bsim, color);
		}

		if (NowhereToGoe.DEBUG) {
			System.out.println("Simulated (Status : " + bsim.getStatus() + " ) " + m + " with score " + ((sco) / boardSpecificSimulations) + "\tabs_score " + sco);
		}
		synchronized (mutex) {
			if (m.equals(stp_move)) {
				sco += boardSpecificSimulations * STP_MOVE_OFFSET;
			}
			if (sco >= max_score) {
				max_score = sco;
				best_move = m;
			}
		}
	}

	/**
	 * Returns if this or the other player won the game
	 *
	 * @param board board to check if this player is winning
	 * @param thisPlayer this player to check if he is winning
	 * @return true if this player won, false otherwise
	 */
	public static boolean isWinning(HexagonalBoard board, PlayerColor thisPlayer) {
		if (board.getStatus() == Status.RedWin) {
			if (thisPlayer == PlayerColor.Red) {
				return true;

			} else {
				return false;

			}
		} else if (thisPlayer == PlayerColor.Blue) {
			return true;

		} else {
			return false;

		}
	}

	/**
	 * Calculates the score of the Agent on an board where some player won Manly
	 * used to get score of for Each simulation to sum up to the score of the
	 * move which is (divided by Simulationcount) the percentage chance of
	 * winning The Value should therefor be between 0 and 100
	 *
	 * 100 for win 0 for loose 50 for nothing
	 *
	 * @param board board wich came to end
	 * @param thisPlayer player get score for
	 * @return 100 win; 0 loose; 50 otherwise
	 */
	public static int getScoreAgent(HexagonalBoard board, PlayerColor thisPlayer) {
		if (board.getStatus() != Status.Ok) {
			if (isWinning(board, thisPlayer)) {
				return 100;

			} else {
				return 0;

			}
		} else {
			return 50;
		}
	}

	/**
	 * Main method to run some tests if needed
	 *
	 * @param args Programmarguments
	 */
	public static void main(String[] args) {
		System.out.println("Run test");
	}
}
