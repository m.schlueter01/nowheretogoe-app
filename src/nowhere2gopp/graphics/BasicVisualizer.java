package nowhere2gopp.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Collection;
import javax.swing.JFrame;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.Viewer;

/**
 * Class to visualize a board or a linklist for debug purposes
 * @author Marius Schlueter
 * @since 2019-06-21
 * @version 1.0
 */
public class BasicVisualizer extends JFrame {

	/**
	 * Serial version from Serializable class
	 */
	public static final long serialVersionUID = 42L;
	/**
	 * Window widht in pixel
	 */
	public static final int W_WIDTH = 800;

	/**
	 * Window height in pixel
	 */
	public static final int W_HEIGHT = 800;

	/**
	 * scale of Sites in pixel
	 */
	public static final int SCALE = 20;//scale of graphical representation of game board

	/**
	 * Gameboard to display
	 */
	Viewer gameBoard = null;

	/**
	 * linklist to display
	 */
	Collection<SiteSet> links = null;

	/**
	 * Constructor for a board
	 * it has always priority to links
	 * @param board board to simulate
	 */
	public BasicVisualizer(Viewer board) {
		if (board == null) {
			throw new NullPointerException("GameBoard to Visualize schouldnt be null");
		}
		gameBoard = board;
		this.setTitle("Nowhere to go");
		this.setSize(W_WIDTH, W_HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Constructor for a board
	 * @param links Links to display
	 */
	public BasicVisualizer(Collection<SiteSet> links) {
		if (links == null) {
			throw new NullPointerException("Links to Visualize schouldnt be null");
		}
		this.links = links;
		this.setTitle("Nowhere to go");
		this.setSize(W_WIDTH, W_HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Paint method draws to window
	 * @param g Some Graphic data
	 */
	@Override
	public void paint(Graphics g) {
		BufferedImage drawing = new BufferedImage(W_WIDTH, W_HEIGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D gg = drawing.createGraphics();
		gg.setColor(new Color(0, 0, 0));
		gg.fillRect(0, 0, W_WIDTH, W_HEIGHT);

		try {

			Site redSite;
			Site blueSite;
			int size = 7;
			if (gameBoard != null) {
				links = gameBoard.getLinks();
				redSite = gameBoard.getAgent(PlayerColor.Red);
				blueSite = gameBoard.getAgent(PlayerColor.Blue);
				size = gameBoard.getSize();
			} else{
				redSite = new Site(0,0);
				blueSite = new Site(0,0);
			}

			for (SiteSet s : links) {

				int xFirst = (size - s.getFirst().getRow()) * SCALE * 2 + (s.getFirst().getColumn()) * SCALE * 4;
				int yFirst = (1 + size - s.getFirst().getRow()) * SCALE * 4;

				int xSecond = (size - s.getSecond().getRow()) * SCALE * 2 + (s.getSecond().getColumn()) * SCALE * 4;
				int ySecond = (1 + size - s.getSecond().getRow()) * SCALE * 4;

				//draw neighbourship lines
				gg.setColor(new Color(255, 255, 255));

				//draw a line into his direction: circle with 2*PI/6 (each neighbour) -> angle of PI/3
				gg.drawLine(xFirst + SCALE / 2, yFirst + SCALE / 2, xSecond + SCALE / 2, ySecond + SCALE / 2);

				gg.setColor(new Color(255, 128, 0));

				//draw circle
				gg.fillOval(xFirst, yFirst, SCALE, SCALE);

				//draw circle
				gg.fillOval(xSecond, ySecond, SCALE, SCALE);
			}

			int xRed = (size - redSite.getRow()) * SCALE * 2 + (redSite.getColumn()) * SCALE * 4;
			int yRed = (1 + size - redSite.getRow()) * SCALE * 4;

			gg.setColor(new Color(255, 0, 0));
			gg.fillOval(xRed, yRed, SCALE, SCALE);

			int xBlue = (size - blueSite.getRow()) * SCALE * 2 + (blueSite.getColumn()) * SCALE * 4;
			int yBlue = (1 + size - blueSite.getRow()) * SCALE * 4;

			gg.setColor(new Color(0, 0, 255));
			gg.fillOval(xBlue, yBlue, SCALE, SCALE);

			g.drawImage(drawing, 0, 0, null);

		} catch (Exception exc) {
			System.err.println(exc);
			for (StackTraceElement s : exc.getStackTrace()) {
				System.err.println(s);
			}
			this.dispose();
		}

	}

	/**
	 * Sends window a message to update asyncronusly
	 */
	public void update() {
		this.revalidate();
		this.repaint();
	}
}
