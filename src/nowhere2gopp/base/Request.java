package nowhere2gopp.base;

import java.io.Serializable;
import java.util.Scanner;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveFormatException;
import nowhere2gopp.preset.PlayerType;
import nowhere2gopp.preset.Requestable;
/**
 * @author Maurice Mueller
 * @since 2019-06-24
 * @version 1.0
 *
 * This class is made to offer the standard input for the human player object.
 * You can use request() to get the move-string by the user and parse it automaticly into a move-object.
 */
public class Request implements Requestable , Serializable{
	private final static long serialVersionUID = 42L;
	
	private PlayerType type;
	/**
	 * This is a simple constructor to create the request-object in dependence of the type of player.
	 *
	 * @param type The type of player
	 */
	public Request(PlayerType type) {
		this.type = type;
	}
	/**
	 * This methods uses the scanner from the standard input to get a move-string from the human user.
	 * Then the move-string will be casted to a Move-Object from preset.
	 * @return A Move-object, read from the standard input
	 */
	public Move request() throws MoveFormatException {
		String input = "";
		try {
			Scanner s = new Scanner(System.in);
			System.out.print("Enter a move in valid syntax:\n-->");
			input = s.nextLine();
		} catch (Exception e) {
			//should not happen
		}
		try {
			return Move.parse(input);
		} catch (Exception e) {
			throw new MoveFormatException(e.getMessage());
		}
	}
}
