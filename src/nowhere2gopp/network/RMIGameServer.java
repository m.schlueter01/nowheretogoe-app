package nowhere2gopp.network;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import nowhere2gopp.graphics.Graphical;
import nowhere2gopp.board.HexagonalBoard;

import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Status;

/**
 * This is the game-server for the whole game. It handles two players.
 *
 * @author Maurice Mueller
 * @since 2019-07-07
 * @version 1.0
 */
public class RMIGameServer extends Thread {

	/**
	 * The ip-address of red (beginning) player
	 */
	private String redIP;

	/**
	 * The ip-address of blue (second) player
	 */
	private String blueIP;

	/**
	 * The port of red (beginning) player
	 */
	private int redPort;

	/**
	 * The port of blue (second) player
	 */
	private int bluePort;

	/**
	 * The size of the game board
	 */
	private int size;

	/**
	 * The delay for two computer players
	 */
	private int delay;

	/**
	 * A constructor which takes the contact data of red and blue player
	 *
	 * @param redIP The ip-address of red (beginning) player
	 * @param blueIP The ip-address of blue (second) player
	 * @param redPort The port of red (beginning) player
	 * @param bluePort The port of blue (second) player
	 * @param delay The delay when two computers play aginst each other
	 * @param size The size of the game board
	 */
	public RMIGameServer(String redIP, String blueIP, int redPort, int bluePort, int delay, int size) {
		this.redIP = redIP;
		this.blueIP = blueIP;
		this.redPort = redPort;
		this.bluePort = bluePort;
		this.delay = delay;
		this.size = size;
		
		System.out.println("size "+size);
	}

	/**
	 * This method is the game server. It contacts both players and requests for moves
	 */
	@Override
	public void run() {
		//implementation of client's registry code
		//contact the network players
		try {
			//connect to address from red player
			System.out.println("CONTROL R1");
			Registry registry = LocateRegistry.getRegistry(this.redIP, this.redPort);
			System.out.println("CONTROL R2");
			String rmi1 = "rmi://" + this.redIP + ":" + this.redPort + "/clientAcceptor";
			System.out.println("rmi1: " + rmi1);
			System.out.println("CONTROL R3");
			Player playerRed = (Player) registry.lookup("clientAcceptor:"+this.redPort);
			System.out.println("CONTROL R4");

			//connect to address from blue player
			System.out.println("CONTROL B1");
			registry = LocateRegistry.getRegistry(this.blueIP, this.bluePort);
			System.out.println("CONTROL B2");
			String rmi2 = "rmi://" + this.blueIP + ":" + this.bluePort + "/clientAcceptor";
			System.out.println("rmi2: " + rmi2);
			System.out.println("CONTROL B3");
			Player playerBlue = (Player) registry.lookup("clientAcceptor:"+this.bluePort);
			System.out.println("CONTROL B4");

			//game logic for two remote players
			//make board
			HexagonalBoard gameBoard = new HexagonalBoard(this.size);
			//make window
			//Graphical window = new Graphical();
			//initialize players
			playerRed.init(this.size, PlayerColor.Red);
			playerBlue.init(this.size, PlayerColor.Blue);
			Status status = Status.Ok;
			Move m;
			//main loop for two human players
			while (status == Status.Ok) {
				//red player
				m = playerRed.request();
				gameBoard.make(m);
				status = gameBoard.getStatus();
				playerRed.confirm(status);
				playerBlue.update(m, status);
				//window.update(gameBoard.viewer());
				Thread.sleep(this.delay);
				//blue player
				if (status == Status.Ok) {
					m = playerBlue.request();
					gameBoard.make(m);
					status = gameBoard.getStatus();
					playerBlue.confirm(status);
					playerRed.update(m, status);
					//window.update(gameBoard.viewer());
					Thread.sleep(this.delay);
				}
			}
		} catch (NotBoundException nbe) {
			//unknown error
			System.err.println("NotBoundException in game loop or contacting the players!");
			nbe.printStackTrace();
			System.exit(1);
		} catch (RemoteException re) {
			//unknown error
			System.err.println("RemoteException in game loop or contacting the players!");
			re.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			//unknown error
			System.err.println("Exception in game loop or contacting the players!");
			e.printStackTrace();
			System.exit(1);
		}
	}
}
