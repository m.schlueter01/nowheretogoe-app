package nowhere2gopp.graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.lang.UnsupportedOperationException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import nowhere2gopp.base.NowhereToGoe;
import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.network.RMIMode;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Requestable;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.SiteTuple;
import nowhere2gopp.preset.Status;
import nowhere2gopp.preset.Viewer;

/**
 * @author Maurice Mueller, Nico Hundertmark
 * @version 3.0
 *
 * Long time ago, Maurice wrote a pre-version of this class to test the first
 * game-window. Since the task-specification was published, Nico wrote some
 * methods to detect where the player wants to set his next move on the game
 * board. Some days before the game has to be ready, Maurice wrote all the
 * comments and repaired the most of the methods in this class to have a better
 * game experience.
 */
public class Graphical extends JFrame implements Requestable, VisualUpdater, MouseListener, ComponentListener {

	/**
	 * The implementation of serialVersionUID from Serializable.
	 */
	public static final long serialVersionUID = 201907091453L;

	/**
	 * The standard window width before the user changes it.
	 */
	public static int WIDTH = 800;

	/**
	 * The standard window height before the user changes it.
	 */
	public static int HEIGHT = 600;

	/**
	 * The scale for the graphical appearance of the game board. It is adapted
	 * automatically.
	 */
	public static int SCALE = 18;

	/**
	 * The shift from left window border to BufferedImage.
	 */
	public static int IMAGE_X_SHIFT = 15;

	/**
	 * The shift from top window border to BufferedImage.
	 */
	public static int IMAGE_Y_SHIFT = 50;

	/**
	 * The game board.
	 */
	private Viewer board;

	/**
	 * A variable to store the color of players.
	 */
	private PlayerColor pColor;

	/**
	 * A variable to store the own player's color.
	 */
	private PlayerColor ownColor;

	/**
	 * A variable to store the network mode to show it in the window.
	 * When it is null, then the game is in local mode.
	 */
	private RMIMode mode = null;

	/**
	 * A SiteSet to store the next move of a player.
	 */
	private SiteSet link = null;

	/**
	 * A SiteSet to store the next move of a player.
	 */
	private SiteSet link2 = null;

	/**
	 * A Site to store the next move of a player.
	 */
	private Site clickedSite = null;

	/**
	 * This is the constructor used by the main-program and this builds the
	 * whole window.
	 *
	 */
	public Graphical() {
		this.setTitle("Nowhere to go");
		//window settings
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(300, 100);
		this.setSize(WIDTH, HEIGHT);
		this.setUndecorated(false);
		//listener
		this.addMouseListener(this);
		//make it visible
		this.setVisible(true);
	}

	/**
	 * This method is to wait for clicks by the user. Therefor the method has
	 * while-loops and waits until the depending variables aren't null anymore.
	 *
	 * @throws Exception in any case of error.
	 * @return A Move-object or null in case of error.
	 */
	public Move request() throws Exception {
		//wait for first link coming from mouseClickEvent
		while (link == null) {
			Thread.yield();
		}
		//wait for second link or site coming from mouseClickEvent
		boolean llPhase = this.board.getPhase() == HexagonalBoard.GamePhase.LinkLink;
		while ((!llPhase && clickedSite == null) || (llPhase && (link2 == null || link2 == link))) {
			Thread.yield();
		}
		if (NowhereToGoe.DEBUG) {
			System.out.println("link: " + link);
			System.out.println("link2: " + link2);
			System.out.println("clickedSite: " + clickedSite);
		}
		//determine: when clickedSite is null, this was second link, else it was a site
		Move retMove;
		if (clickedSite == null) {
			//link2 was defined in second while loop
			retMove = new Move(link, link2);
			link2 = null;
		} else {//in other case link2 == null
			//clickedSite was defined in second while loop
			retMove = new Move(new SiteTuple(board.getAgent(pColor), clickedSite), link);
			clickedSite = null;
		}
		link = null;
		System.out.println("Move made by " + (this.pColor == PlayerColor.Red ? "red" : "blue") + " player: " + retMove);
		return retMove;
	}

	/**
	 * This method is to update the status and show a dialog box in case of a
	 * winning player.
	 *
	 * @param board The game board.
	 */
	public void update(Viewer board) {
		this.board = board;
		pColor = board.getTurn();
		switch (board.getStatus()) {
			case BlueWin:
			case RedWin:
				String won = (board.getStatus() == Status.BlueWin ? "Blue" : "Red") + " won the game!";
				JOptionPane.showMessageDialog(null, won, "Game finished", JOptionPane.PLAIN_MESSAGE);
				break;
			default:
			//nothing to do
		}
		this.repaint();
	}

	/**
	 * This is the main graphical unit to show the game board and the players.
	 *
	 * @param g The graphics object.
	 */
	@Override
	public void paint(Graphics g) {

		if (this.board == null) {
			return;
		}

		//some colors
		Color RED = new Color(196, 0, 0);
		Color GREEN = Color.GREEN;
		Color BLUE = Color.BLUE;
		Color ORANGE = new Color(255, 128, 0);
		Color WHITE = Color.WHITE;
		Color GRAY = Color.GRAY;
		Color BG_RED = new Color(128, 25, 25);
		Color BG_BLUE = new Color(25, 25, 128);

		//20 is the factor between SCALE and window size
		int w = this.getWidth() / (5 * this.board.getSize());
		int h = this.getHeight() / (5 * this.board.getSize());
		SCALE = (w < h ? w : h);

		//initialize virtual canvas
		BufferedImage img = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D g2D = img.createGraphics();

		//make background in dependence of player color
		if (this.ownColor == PlayerColor.Red) {
			g2D.setColor(BG_RED);
		} else {
			g2D.setColor(BG_BLUE);
		}
		g2D.fillRect(0, 0, WIDTH, HEIGHT);

		//draw which player's turn it is
		String txt = "";
		int textWidth = 0;
		switch (this.pColor) {
			case Red:
			case Blue:
				if (this.pColor == PlayerColor.Red) {//red player's turn
					g2D.setColor(RED);
					txt = "Red players turn!";
				} else {//blue player's turn
					g2D.setColor(BLUE);
					txt = "Blue players turn!";
				}
				textWidth = g.getFontMetrics().stringWidth(txt);
				g2D.drawString(txt, (this.getWidth() - textWidth) / 2, 13);
				break;
			default:
			//nothing to do
		}

		//show game phase
		txt = "GamePhase: " + this.board.getPhase() + ", Game status: " + this.board.getStatus();
		textWidth = g.getFontMetrics().stringWidth(txt);
		g2D.drawString(txt, (this.getWidth() - textWidth) / 2, 26);

		//show who the player is
		txt = "YOU ARE THE " + (this.ownColor == PlayerColor.Red?"RED":"BLUE") + " PLAYER!";
		textWidth = g.getFontMetrics().stringWidth(txt);
		g2D.drawString(txt, (this.getWidth() - textWidth) / 2, 39);

		//show the network mode
		txt = "Network mode: ";
		if (mode != null) {
			if (mode == RMIMode.FullServer) {
				txt += "FullServer";
			} else if (mode == RMIMode.HalfServer ) {
				txt += "HalfServer";
			} else if (mode == RMIMode.Client) {
				txt += "Client";
			}
		} else {
			txt += "Local";
		}
		textWidth = g.getFontMetrics().stringWidth(txt);
		g2D.drawString(txt, (this.getWidth() - textWidth) / 2, 52);

		//the player sites to compare and set the fitting colors
		Site redSite = this.board.getAgent(PlayerColor.Red);
		Site blueSite = this.board.getAgent(PlayerColor.Blue);

		try {
			for (SiteSet s : this.board.getLinks()) {

				//calculate the start- and end-positions of links
				int xF = this.getX(s.getFirst());
				int yF = this.getY(s.getFirst());
				int xS = this.getX(s.getSecond());
				int yS = this.getY(s.getSecond());

				//"SCALE / 2" is the radius of the fields
				int r = SCALE / 2;//radius

				//draw links in fitting color
				if (s.equals(link)) {
					//link is selected link by user
					g2D.setColor(GREEN);
				} else {
					//link isn't selected
					g2D.setColor(GRAY);
				}

				//draw site
				g2D.setStroke(new BasicStroke(3));
				g2D.drawLine(xF + r, yF + r, xS + r, yS + r);
				g2D.setStroke(new BasicStroke(1));

				//players only were drawn when LinkLink-phase is over
				boolean val = this.board.getPhase() == HexagonalBoard.GamePhase.AgentLink;//val = visible agent link
				boolean vsa = this.board.getPhase() == HexagonalBoard.GamePhase.SetAgent;//first set position of players, vsa = visible set agent

				//draw first field
				if (s.getFirst().equals(clickedSite)) {
					g2D.setColor(GREEN);
				} else if (s.getFirst().equals(redSite) && (val || (vsa && pColor == PlayerColor.Blue))) {
					g2D.setColor(RED);
				} else if (s.getFirst().equals(blueSite) && val) {
					g2D.setColor(BLUE);
				} else {
					g2D.setColor(ORANGE);
				}
				g2D.fillOval(xF, yF, SCALE, SCALE);

				//draw second field
				if (s.getSecond().equals(clickedSite)) {
					g2D.setColor(GREEN);
				} else if (s.getSecond().equals(redSite) && (val || (vsa && pColor == PlayerColor.Blue))) {
					g2D.setColor(RED);
				} else if (s.getSecond().equals(blueSite) && val) {
					g2D.setColor(BLUE);
				} else {
					g2D.setColor(ORANGE);
				}
				g2D.fillOval(xS, yS, SCALE, SCALE);
			}

			//redraw white background on window to avoid black borders in the corners of the window
			//make background in dependence of player color
			if (this.ownColor == PlayerColor.Red) {
				g.setColor(BG_RED);
			} else {
				g.setColor(BG_BLUE);
			}
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			//draw virtual canvas on window
			g.drawImage(img, IMAGE_X_SHIFT, IMAGE_Y_SHIFT, this.getWidth(), this.getHeight(), null);
		} catch (Exception e) {
			//this.dispose();
		}
	}

	/**
	 * This method is a helper method to calculate the x position of a site.
	 *
	 * @param s A Site to calculate the position from.
	 * @return the x-position of that Site.
	 */
	private int getX(Site s) {
		return (2 * (this.board.getSize() - s.getRow()) + 4 * s.getColumn()) * SCALE;
	}

	/**
	 * This method is a helper method to calculate the y position of a site.
	 *
	 * @param s A Site to calculate the position from.
	 * @return the y-position of that Site.
	 */
	private int getY(Site s) {
		return (int) (2 * Math.sqrt(3) * (this.board.getSize() - s.getRow()) * SCALE);
	}

	/**
	 * This method is to set game board manually.
	 *
	 * @param board The game board
	 */
	public void setGameBoard(HexagonalBoard board) {
		this.board = board;
	}

	/**
	 * This method is to set the user's player color got from the server.
	 *
	 * @param col The color
	 */
	public void setPlayerColor(PlayerColor col) {
		this.ownColor = col;
		this.pColor = col;
	}

	/**
	 * This method is to set the network mode to show it in the window.
	 *
	 * @param mode The network mode
	 */
	public void setNetworkMode(RMIMode mode) {
		this.mode = mode;
	}

	/**
	 * Method to calculate the shortest distance from a piont to a line
	 *
	 * @param x1 x-coordinate from line point A
	 * @param y1 y-coordinate from line point A
	 * @param x2 x-coordinate from line point B
	 * @param y2 y-coordinate from line point B
	 * @param x3 x-coordinate from point P
	 * @param y3 y-coordinate from point P
	 * @return double the distance between a point P and a line with two points
	 * A and B
	 */
	private double shortestDistance(double x1, double y1, double x2, double y2, double x3, double y3) {
		double px = x2 - x1;
		double py = y2 - y1;
		double temp = (px * px) + (py * py);
		double u = ((x3 - x1) * px + (y3 - y1) * py) / temp;
		if (u > 1) {
			u = 1;
		} else if (u < 0) {
			u = 0;
		}
		double x = x1 + u * px;
		double y = y1 + u * py;
		double dx = x - x3;
		double dy = y - y3;
		double dist = (dx * dx + dy * dy);
		return dist;
	}

	/**
	 * This method is to find links on the game board to detect what the user
	 * has clicked.
	 *
	 * @param x The x-position of mouse pointer.
	 * @param y The y-position of mouse pointer.
	 * @return A SiteSet or null in case of no fitting sitesets
	 */
	private SiteSet findLink(int x, int y) {
		boolean found = false;
		for (SiteSet s : board.getLinks()) {
			//if nothing was found until now go ahead
			if (!found) {

				//calculate the start- and end-positions of links
				int xF = this.getX(s.getFirst()) + IMAGE_X_SHIFT;
				int yF = this.getY(s.getFirst()) + IMAGE_Y_SHIFT;
				int xS = this.getX(s.getSecond()) + IMAGE_X_SHIFT;
				int yS = this.getY(s.getSecond()) + IMAGE_Y_SHIFT;

				if (shortestDistance((xF + SCALE * .5), (yF + SCALE * .5), (xS + SCALE * .5), (yS + SCALE * .5), x, y) <= 20) {
					return new SiteSet(s.getFirst(), s.getSecond());
				}
			} else {
				break;
			}
		}
		return null;
	}

	/**
	 * This method is to find sites on the game board to detect what the user
	 * has clicked.
	 *
	 * @param x The x-position of mouse pointer.
	 * @param y The y-position of mouse pointer.
	 * @return A Site or null in case of no fitting site
	 */
	private Site findSite(int x, int y) {
		for (SiteSet s : board.getLinks()) {

			//calculate the start- and end-positions of links
			int xF = this.getX(s.getFirst()) + IMAGE_X_SHIFT;
			int yF = this.getY(s.getFirst()) + IMAGE_Y_SHIFT;
			int xS = this.getX(s.getSecond()) + IMAGE_X_SHIFT;
			int yS = this.getY(s.getSecond()) + IMAGE_Y_SHIFT;

			if (x >= xF && x <= xF + SCALE && y >= yF && y <= yF + SCALE) {
				return s.getFirst();
			} else if (x >= xS && x <= xS + SCALE && y >= yS && y <= yS + SCALE) {
				return s.getSecond();
			}
		}
		//when null is returned, this method is called again
		return null;
	}

	/**
	 * This method is to handle the mouse clicks and defines the actions.
	 *
	 * @param me A mouse event, when the mouse button was clicked.
	 */
	@Override
	public void mouseClicked(MouseEvent me) {
		if (link == null) {
			SiteSet testLink = findLink(me.getX(), me.getY());
			if (testLink != null && findSite(me.getX(), me.getY()) == null) {
				link = testLink;
			} else {
				//link was a site too, when site wasn't null
			}
		} else if (link2 == null || clickedSite == null) {
			if (this.board.getPhase() == HexagonalBoard.GamePhase.LinkLink) {
				SiteSet testLink = findLink(me.getX(), me.getY());
				if (testLink != null && !(testLink.equals(link)) && findSite(me.getX(), me.getY()) == null) {
					link2 = testLink;
				} else {
					//the same links aren't allowed
				}
			} else {
				//the player sites to compare and set the fitting colors
				Site redSite = this.board.getAgent(PlayerColor.Red);
				Site blueSite = this.board.getAgent(PlayerColor.Blue);

				//compare testSite with site of players to avoid to set the player on an unfree place.
				Site testSite = findSite(me.getX(), me.getY());
				if (redSite != testSite && blueSite != testSite) {
					//in setagent the sites can be null, then the player has free choise
					clickedSite = testSite;
				} else {
					//It's forbidden to set a player on a place where an other player sits
				}
			}
		}
		if (NowhereToGoe.DEBUG) {
			System.out.println(me.getX() + " " + me.getY());
			System.out.println("link: " + link + " link2: " + link2 + " clickSite: " + clickedSite);
		}
		this.repaint();
	}

	/**
	 * @param me A mouse event, when the mouse button was pressed down.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void mousePressed(MouseEvent me) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * @param me A mouse event, when the mouse button was released.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void mouseReleased(MouseEvent me) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * @param me A mouse event, when the mouse entered a component.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void mouseEntered(MouseEvent me) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * @param me A mouse event, when the mouse left a component.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void mouseExited(MouseEvent me) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * @param ce A component event, when the window is hidden.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void componentHidden(ComponentEvent ce) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * @param ce A component event, when the window is shown.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void componentShown(ComponentEvent ce) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * @param ce A component event, when the window is moved.
	 * @throws UnsupportedOperationException Not supported yet.
	 */
	@Override
	public void componentMoved(ComponentEvent ce) throws UnsupportedOperationException {
		//false is a protection to avoid this exception for first
		if (NowhereToGoe.DEBUG && false) {
			throw new UnsupportedOperationException("Not implemented yet!");
		}
	}

	/**
	 * This method is to handle resized windows.
	 *
	 * @param ce A component event, when the window is resized.
	 */
	@Override
	public void componentResized(ComponentEvent ce) {
		this.repaint();
	}
}
