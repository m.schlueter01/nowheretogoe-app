=================================================================
GENERAL INFORMATION ABOUT PARAMETERS FOR START CLASS NOWHERETOGOE
=================================================================
GNERAL
	--debug
		Set this flag to show all debug information.
	--help
		Show this help page.
	-networkMode "fullServer" | "halfServer" | "client"
		"fullServer" -> server mode without local player.
		"halfServer" -> server mode with one local player.
		"client" -> client mode with either one or two local players.
	-delay [milliseconds]
		Set the reaction time in milliseconds for any
		computer players. So when you play as human with a
		computer, it will wait for this delay. When two
		computer players play against each other, both
		computers will wait for this delay.
		Default: 300
	-size [3, 4, 5, 6, 7]
		Set the size of the middle line of the game board.
		Default: 7

RED PLAYER (beginning player)
	-red [See below]
		Set the player type for beginning player.
		Default: human
	-redIP [xxx.xxx.xxx.xxx] or hostname
		If red player is remote player, set his/her/its ip-address.
		Default: localhost
	-redPort [0, ..., 65536]
		If red player is remote player, set his/her/its port number.
		Default: 80
	-redInputMethod "tui" | "gui"
		If red player is a human player, set his/her/its
		input mode to text-based or graphic-based.
		Default: "tui"

BLUE PLAYER
	-blue
		Set the player type.
		Default: human
	-blueIP [xxx.xxx.xxx.xxx] or hostname
		If blue player is remote player, set his/her/its ip-address.
		Default: localhost
	-bluePort [0, ..., 65536]
		If blue player is remote player, set his/her/its port number.
		Default: 80
	-blueInputMethod  "tui" | "gui"
		If blue player is a human player, set his/her/its
		input mode to text-based or graphic-based.
		Default: "tui"

MONTE CARLO
	-presimRange <u_int>
		The maximal number of moves calculated per simulation.
		Default: 50
	-simulations <u_int>
		The number of simulations for each move.
		Default: 20
	-potentialSites <u_int>
		The number of random sites to calculate the move for.
		Default: 50
	-simScaling <u_int>
		The dcaling value of simulations in dependency of own-position-weight.
		Default: 300
	-randomActivationDivisor <u_int>
		The randomness factor in calculations.
		Default: 10
	-advancedPrecalcMoves <u_int>
		The initial number of taken moves to simulate with.
		Default: 10
	-stpMoveOffset <u_int>
		The percentage offset of StrategicPlayer's move.
		Default: 0

PLAYER TYPES (used for -red and -blue)
 human -> HumanPlayer
 random -> RandomPlayer
 simple -> WinningComputerPlayer
 extended -> StrategicComputerPlayer
 upgraded -> MonteCarloPlayer
 enhanced -> /
 advanced -> /
 remote -> NetworkPlayer

 [written by Maurice Mueller]
=================================================================
