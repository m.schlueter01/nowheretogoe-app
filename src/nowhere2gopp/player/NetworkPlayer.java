package nowhere2gopp.player;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import nowhere2gopp.board.BoardInformation;
import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Status;
import nowhere2gopp.preset.Viewable;
import nowhere2gopp.preset.Viewer;

/**
 * Class representing a network player who can wrapp any other player
 * implementation to an rmi server/client
 * @author Marius Schlueter, Maurice Mueller
 */
public class NetworkPlayer extends UnicastRemoteObject implements Player, Viewable {

	/**
	 * Implemented for Serializable
	 */
	private static final long serialVersionUID = 20190702212400L;

	/**
	 * The player to be offered
	 */
	private Player player = null;

	/**
	 * A game board to log moves to display it later
	 */
	private HexagonalBoard board = null;

	/**
	 * Constructor to generate this NetworkPlayer
	 * @param p Player to wrap
	 * @throws IllegalArgumentException if player is null
	 * @throws RemoteException Is thrown when NetworkPlayer fails to build object
	 */
	public NetworkPlayer(Player p) throws RemoteException {
		if (p == null) {
			throw new IllegalArgumentException("Player is null");
		}
		player = p;
	}

	/**
	 * Initializes the player
	 * @param boardSize size
	 * @param color color
	 * @throws Exception init failed bc size not valid
	 * @throws RemoteException network
	 */
	@Override
	public void init(int boardSize, PlayerColor color) throws Exception, RemoteException {
		player.init(boardSize, color);
		board = new HexagonalBoard(boardSize);
	}

	/**
	 * Requests move from wrapped player
	 * @return move from player
	 * @throws Exception if move is not valid
	 * @throws RemoteException for network
	 */
	@Override
	public Move request() throws Exception, RemoteException {
		Move m = player.request();
		board.make(m);
		return m;
	}

	/**
	 * Confirms move from wrapped player
	 * @param status status to confirm
	 * @throws Exception from wrapped player
	 * @throws RemoteException noetwork
	 */
	@Override
	public void confirm(Status status) throws Exception, RemoteException {
		player.confirm(status);
	}

	/**
	 * Updates move from wrapped player
	 * @param opponentMove move
	 * @param status statue
	 * @throws Exception move not valid
	 * @throws RemoteException network
	 */
	@Override
	public void update(Move opponentMove, Status status) throws Exception, RemoteException {
		player.update(opponentMove, status);
		board.make(opponentMove);
	}

	/**
	 * Returns a viewer object of own game board
	 * @return a viewer
	 */
	@Override
	public Viewer viewer() {
		return new BoardInformation(board);
	}
}
