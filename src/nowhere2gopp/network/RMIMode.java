package nowhere2gopp.network;

/**
 * This is an enumeration to store the different types of RMI modes. Use them to differenciate between game
 * modes when you start the game.
 *
 * @author Maurice Mueller
 * @since 2019-07-04
 * @version 1.0
 */
public enum RMIMode {
	FullServer,
	HalfServer,
	Client
}
