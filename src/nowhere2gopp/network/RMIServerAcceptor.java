package nowhere2gopp.network;

import java.lang.NullPointerException;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import nowhere2gopp.base.NowhereToGoe;
import nowhere2gopp.network.RMIGameServer;
import nowhere2gopp.preset.Player;

/**
 * This is the rmi-server for the server side of the whole game
 *
 * @author Maurice Mueller
 * @since 2019-07-04
 * @version 1.0
 */
public class RMIServerAcceptor {
	private static Player red = null;
	private static Player blue = null;
	private static String redIP = "";
	private static String blueIP = "";
	private static int redPort = 0;
	private static int bluePort = 0;
	private static int size = 3;
	private static int delay = 0;

	/**
	 * A standard constuctor for an RMIServer
	 *
	 * @throws RemoteException in case of remote error
	 */
	public RMIServerAcceptor() throws RemoteException {
		startRMIServer();
	}

	/**
	 * This method sets the size of the game board
	 *
	 * @param s The size of the game board
	 */
	public static void setSize(int s) {
		size = s;
	}

	/**
	 * This method sets the delay for the case of two computer players
	 *
	 * @param d The delay when two computer players play
	 */
	public void setDelay(int d) {
		delay = d;
	}

	/**
	 * Start this method to setup the RMI-Server.
	 * Give a player when one local player exists, else null
	 * This method is used by the server module.
	 *
	 * @param p an existing player or null
	 * @param ip The ip address of player
	 * @param port The port number of player
	 * @throws NullPointerException when given player is null
	 */
	public static void addPlayer(Player p, String ip, int port) throws NullPointerException {
		if (p != null) {
			System.out.println("Added player " + (red==null?"red":"blue") + " at ip: " + ip + " and port: " + port + ".");
			if (red == null) {
				redIP = ip;
				redPort = port;
				red = p;
			} else if (blue == null) {
				blueIP = ip;
				bluePort = port;
				blue = p;
			}
		} else {
			throw new NullPointerException("Player is null!");
		}
		if (red != null && blue != null) {
			startGameServer();
		}
	}

	/**
	 * Starts the server with the game logic in new thread to be able to terminate
	 * while the game is running. Two players should be available here and they will be used in the thread.
	 */
	public static void startGameServer() {
		//make new thread
		RMIGameServer rgs = new RMIGameServer(redIP, blueIP, redPort, bluePort, delay, size);

		//show some information
		if (NowhereToGoe.DEBUG) {
			System.out.println("-  redIP: " + redIP);
			System.out.println("-  blueIP: " + blueIP);
			System.out.println("-  redPort: " + redPort);
			System.out.println("-  bluePort: " + bluePort);
		}

		//start server
		rgs.start();
		System.out.println("Game server started with two players!");
	}

	/**
	 * Starts the rmi server to get two players.
	 *
	 * @throws RemoteException in case of remote error
	 */
	public static void startRMIServer() throws RemoteException {
		//constant for shorter code
		int STD_PORT = Registry.REGISTRY_PORT;//standard port 1099
		//create registry
		int freePort = STD_PORT;
		//while-loop is terminated by if-block
		while (true) {
			try {
				LocateRegistry.createRegistry(freePort);
				break;
			} catch (RemoteException re) {
				if (NowhereToGoe.DEBUG) {
					System.err.println("================");
					re.printStackTrace();
					System.err.println("================");
					System.err.println("Can't bind serverAcceeptor to port " + freePort + ".");
				}
				freePort++;
			} catch (Exception e) {
				e.printStackTrace();
				freePort++;
			}
			if (freePort >= 1102) {
				System.err.println("Can't bind serverAcceptor to ports between " + STD_PORT + " and " + freePort + ".");
				System.exit(1);
			}
		}
		if (NowhereToGoe.DEBUG) {
			System.out.println("Server acceptor bound to " + freePort + ".");
		}
		//create accepting module
		RMIServerAcceptorModule rsam = new RMIServerAcceptorModule();
		if (NowhereToGoe.DEBUG) {
			System.out.println("Prepared server module successfully.");
		}
		RMIServerAcceptorInterface rsai = (RMIServerAcceptorInterface)(UnicastRemoteObject.exportObject(rsam, 0));
		if (NowhereToGoe.DEBUG) {
			System.out.println("Exports server module successfully.");
		}
		//set standard log output to standard output
		RemoteServer.setLog(System.out);
		if (NowhereToGoe.DEBUG) {
			System.out.println("Loaded server module successfully.");
		}
		//load accepting module
		//set server online
		try {
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("serverAcceptor", rsai);
		} catch (RemoteException re) {
			System.err.println("Error in creating registry!");
			System.exit(1);
		} catch (AlreadyBoundException abe) {
			System.err.println("Server Already bound!");
			System.exit(1);
		}
		if (NowhereToGoe.DEBUG) {
			System.out.println("Server was built successfully.");
		}
		//create accepting module
	}

	/**
	 * @return true when red player is null
	 */
	public static boolean existsRedPlayer() {
		return red != null && !redIP.equals("") && redPort != 0;
	}

	/**
	 * @return true when blue player is null
	 */
	public static boolean existsBluePlayer() {
		return blue != null && !blueIP.equals("") && bluePort != 0;
	}

}
