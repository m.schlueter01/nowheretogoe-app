package nowhere2gopp.player;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Iterator;
import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.board.NodeAnalytics;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveType;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.SiteTuple;
import nowhere2gopp.preset.Status;

/**
 * Random move generating Player
 */
public class RandomPlayer extends BasePlayer {

	/**
	 * Calculates valid random moves
	 *
	 * @return random moves
	 * @throws Exception if move not valid
	 * @throws RemoteException network
	 */
	@Override
	public Move request() throws Exception, RemoteException {
		if (pfcs != PlFuncCallState.request) {
			throw new IllegalStateException("Tried call request before update");
		}

		if (board.getStatus() != Status.Ok) {
			throw new IllegalStateException("tryed request move but status = " + board.getStatus());
		}

		Move next = RandomPlayer.getRandomMove(board, board.getAgent(color), board.getAgent(oppColor));

		board.make(next);

		pfcs = PlFuncCallState.confirm;

		return next;
	}

	/**
	 * Calculates a random (but valid) move
	 *
	 * @param board gameBoard
	 * @param agent agent
	 * @param opp opponent
	 * @return a random Move
	 */
	public static Move getRandomMove(HexagonalBoard board, Site agent, Site opp) {

		// next move
		Move next = new Move(MoveType.End);

		NodeAnalytics reachableDataAgent = HexagonalBoard.getReachable(board.getLinks(), agent, opp);
		NodeAnalytics reachableDataEnemy = HexagonalBoard.getReachable(board.getLinks(), opp, agent);

		SiteSet linkToRemove = getRandomRemoveLink(board.getLinks(), reachableDataEnemy.reachableLinks);

		// first link to remove
		// second link if link link
		if (board.getPhase() == HexagonalBoard.GamePhase.LinkLink) {

			SiteSet link2ToRemove = null;

			while (link2ToRemove == null) {
				for (SiteSet s : board.getLinks()) {
					if (Math.random() > 0.9 && s != linkToRemove) {
						link2ToRemove = s;
					}
				}
			}

			next = new Move(linkToRemove, link2ToRemove);

		} else if (board.getPhase() == HexagonalBoard.GamePhase.SetAgent) {

			// Calculate new agent move
			SiteSet sideToMove = null;

			while (sideToMove == null) {
				for (SiteSet s : board.getLinks()) {
					if (Math.random() > 0.9) {
						sideToMove = s;
					}
				}
			}

			next = new Move(new SiteTuple(agent, sideToMove.getFirst()), linkToRemove);

		} else if (board.getPhase() == HexagonalBoard.GamePhase.AgentLink) {

			// Calculate new agent move
			SiteSet sideToMove = null;

			if (reachableDataAgent.allReachableSites.isEmpty()) {
				return new Move(MoveType.End);
			}

			while (sideToMove == null) {
				for (Site s : reachableDataAgent.allReachableSites) {
					if (Math.random() > 0.9) {
						if (!s.equals(opp) && !s.equals(agent)) {
							sideToMove = new SiteSet(agent, s);
							break;
						}
					}
				}
			}

			if (sideToMove.getFirst() == agent) {
				next = new Move(new SiteTuple(agent, sideToMove.getSecond()), linkToRemove);
			} else if (sideToMove.getSecond() == agent) {
				next = new Move(new SiteTuple(agent, sideToMove.getFirst()), linkToRemove);
			}
		}

		return next;
	}

	/**
	 * Removes a link and deletes it from enemyLinks list.If enemyLinks is empty
	 * a random link is taken from alllinks. if you only want a random link from
	 * allLinks you can set enemyLink = null All links wont be changed by this
	 * function
	 *
	 * @param allLinks evry link in game
	 * @param enemyLinks links available by enemy
	 * @return random link to remove
	 */
	public static SiteSet getRandomRemoveLink(Collection<SiteSet> allLinks, Collection<SiteSet> enemyLinks) {
		if (allLinks.isEmpty()) {
			return null;
		}

		SiteSet linkToRemove = null;

		while (linkToRemove == null && enemyLinks != null && !enemyLinks.isEmpty()) {
			for (SiteSet s : enemyLinks) {
				if (Math.random() > 0.9) {
					linkToRemove = s;
				}
			}
		}

		if (linkToRemove != null) {
			enemyLinks.remove(linkToRemove);
		} else {
			while (linkToRemove == null && !allLinks.isEmpty()) {
				for (SiteSet s : allLinks) {
					if (Math.random() > 0.9) {
						linkToRemove = s;
					}
				}
			}
		}
		return linkToRemove;
	}

	public static Site getRandomSite(Collection<Site> sites,Site agent) {
		int random = (int) (Math.random() * (sites.size() - 1));
		Iterator<Site> myits = sites.iterator();
		Site nextNode = myits.next();
		for (int rc = 0; rc < random; rc++) {
			Site n = myits.next();
			if (!n.equals(agent)) {
				nextNode = n;
			}
		}
		if(nextNode == null) throw new RuntimeException("node is null");
		return nextNode;
	}
	
	public static SiteSet getRandomLink(Collection<SiteSet> sites) {
		int random = (int) (Math.random() * (sites.size() - 1));
		Iterator<SiteSet> myits = sites.iterator();
		SiteSet link = myits.next();
		for (int rc = 0; rc < random; rc++) {
			link = myits.next();
		}
		if(link == null) throw new RuntimeException("link is null");
		return link;
	}
}
