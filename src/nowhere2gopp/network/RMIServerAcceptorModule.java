package nowhere2gopp.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

import nowhere2gopp.network.RMIServerAcceptorInterface;
import nowhere2gopp.network.RMIServerAcceptor;
import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;

/**
 * This is a class implementing the concerning interface to be an RMI-server-module. It is able to add a new
 * player to the server
 *
 * @author Maurice Mueller
 * @since 2019-07-03
 * @version 1.0
 */
public class RMIServerAcceptorModule implements RMIServerAcceptorInterface {
	/**
	 * This method is used by a player/client program to add a remote-player to a server
	 *
	 * @param p The player obeject to use
	 * @param ip The ip address where the server can reach the client-RMI-server
	 * @param port The port number where the server can reach the client-RMI-server
	 * @throws RemoteException in case of remote-error
	 * @return a player color (the color of the current player as information
	 */
	@Override
	public PlayerColor addPlayer(Player p, String ip, int port) throws RemoteException {
		//if the following functions give true, the player exists, else they don't exist
		boolean red = RMIServerAcceptor.existsRedPlayer();
		boolean blue = RMIServerAcceptor.existsBluePlayer();
		if (!blue) {
			if (!red) {
				System.out.println("You are the red (beginning) player!");
			} else {
				System.out.println("You are the blue (second) player!");
			}
			//add player to server
			RMIServerAcceptor.addPlayer(p, ip, port);
			if (!red) {
				return PlayerColor.Red;
			} else {
				return PlayerColor.Blue;
			}
		} else {
			if (!red) {
				System.err.println("There is no beginning player!");
			} else {
				System.err.println("There two players yet!");
			}
			return null;
		}
	}
}
