package nowhere2gopp.player;

import java.io.Serializable;
import java.rmi.RemoteException;
import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Status;

/**
 * Base class with basic functionality for all players
 * @author Marius Schlueter
 */
public abstract class BasePlayer implements Player , Serializable{

	/**
	 * It's to implement the Serializable correctly.
	 */
	public static final long serialVersionUID = 201907191000L;

	/**
	 * Enumerationa type specifing the state of the call seqence
	 */
    public enum PlFuncCallState {
        init,
        request,
        confirm,
        update
    }

	/**
	 * Board wher Player logs moves
	 */
    protected HexagonalBoard board = null;
	/**
	 * Own player color
	 */
    protected PlayerColor color = null;
	/**
	 * Enemy player color
	 */
    protected PlayerColor oppColor = null;
	/**
	 * functioncall sequence variable
	 */
    protected PlFuncCallState pfcs = PlFuncCallState.init;

    /**
	 * Confirm state of prev move
	 * @param status status should be equal
	 * @throws Exception if state doesnt equal own state or call is not in sequence
	 * @throws RemoteException for network player
	 */
    @Override
    public void confirm(Status status) throws Exception, RemoteException {
        if (pfcs != PlFuncCallState.confirm) {
            throw new IllegalStateException("Tried call confirm before request");
        }
        if (board.getStatus() != status) {
            throw new IllegalStateException("Iternal state (" + board.getStatus() + ") diverges other state (" + status + ")");
        }
        pfcs = PlFuncCallState.update;
    }

	/**
	 * Update a move from enemy
	 * @param opponentMove the move of the enemy
	 * @param status status from the enemy player
	 * @throws Exception if call is not in sequence or state not own state after makeing the move
	 * @throws RemoteException for network player
	 */
    @Override
    public void update(Move opponentMove, Status status) throws Exception, RemoteException {
        if (pfcs != PlFuncCallState.update) {
            throw new IllegalStateException("Tried call update before confirm");
        }
        board.make(opponentMove);
        if (status != board.getStatus()) {
            throw new IllegalStateException("Iternal state (" + board.getStatus() + ") diverges other state (" + status + ")");
        }
        pfcs = PlFuncCallState.request;
    }

	/**
	 * Initializes Player and gives it a color and its boardsize
	 * @param boardSize size of board
	 * @param color color of this player
	 * @throws Exception if size not valid
	 * @throws RemoteException for network player
	 */
    @Override
    public void init(int boardSize, PlayerColor color) throws Exception, RemoteException {
        board = new HexagonalBoard(boardSize);
        this.color = color;
        this.oppColor = (color == PlayerColor.Blue ? PlayerColor.Red : PlayerColor.Blue);
        if (color == PlayerColor.Red) {
            pfcs = PlFuncCallState.request;
        } else {
            pfcs = PlFuncCallState.update;
        }
    }

}
