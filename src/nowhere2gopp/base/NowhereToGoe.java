package nowhere2gopp.base;

import java.io.File;
import java.io.IOException;
import java.lang.InterruptedException;
import java.lang.UnsupportedOperationException;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.NoSuchElementException;

import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.graphics.Graphical;
import nowhere2gopp.network.RMIServerAcceptor;
import nowhere2gopp.network.RMIMode;
import nowhere2gopp.network.RMIClientAcceptor;
import nowhere2gopp.player.HumanPlayer;
import nowhere2gopp.player.MonteCarloPlayer;
import nowhere2gopp.player.NetworkPlayer;
import nowhere2gopp.player.RandomPlayer;
import nowhere2gopp.player.StrategicComputerPlayer;
import nowhere2gopp.player.WinningComputerPlayer;
import nowhere2gopp.preset.ArgumentParser;
import nowhere2gopp.preset.ArgumentParserException;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveType;
import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.PlayerType;
import nowhere2gopp.preset.Requestable;
import nowhere2gopp.preset.Status;
import nowhere2gopp.preset.Viewer;

/**
 * This is the main class and start class of the whole project. Use it to start server, client or local game.
 * Use "--help" to get some help about parameters and settings.
 *
 * @author Maurice Mueller
 * @since 2019-06-17
 * @version 1.0
 */
public class NowhereToGoe {

	// IMPORTANT! All these constants will be set later with command line arguments or standard values
	// All these constants aren't final to set them with ArgumentParser. So don't change them to final!
	/**
	 * All log- and debug-outputs can be shown by setting the -debug flag. In
	 * all java classes of this project you can use <b>if (NowhereToGoe.DEBUG)
	 * {}</b> to show a text in dependence of this flag.
	 */
	public static boolean DEBUG;

	/**
	 * If the flag -help is set, the game only will show the help page from
	 * "argumentStandards.txt" and terminates.
	 */
	public static boolean HELP;

	/**
	 * The size is the length of the game board in the longest (middle) row. It
	 * has to be an odd number between 3 and 11.
	 */
	public static int SIZE;

	/**
	 * When this is set to "fullServer", it will wait for two players in
	 * network. When this is set to "half_server", it will take the red player
	 * and will wait for a network player. When this is set to "client", it will
	 * take two players with following possibilities: REMOTE-LOCAL send the
	 * local player with remote to RMI-OUTPUT and display from RMI-INPUT.
	 * LOCAL-LOCAL use the two players for normal game loop.
	 */
	public static RMIMode NETWORK_MODE;
	
	/**
	 * This is the delay when one of the players or both players are any type of
	 * computer player. The delay is the number of milliseconds until the next
	 * computer-decided move is executed.
	 */
	public static int MOVE_SLEEP_TIME;

	/**
	 * Type of red (beginning) player.
	 */
	public static PlayerType PLAYER_TYPE_RED;

	/**
	 * When red player is a network player, then this is his/her/its ip-address.
	 */
	public static String PLAYER_RED_IP;

	/**
	 * When red player is a network player, then this is his/her/its port.
	 */
	public static int PLAYER_RED_PORT;

	/**
	 * When red player is human player, decide between textbased user interface
	 * ("tui") or graphical user interface ("gui")
	 */
	public static String RED_INPUT_METHOD;

	/**
	 * Type of blue (not beginning) player.
	 */
	public static PlayerType PLAYER_TYPE_BLUE;

	/**
	 * When blue player is a network player, then this is his/her/its
	 * ip-address.
	 */
	public static String PLAYER_BLUE_IP;

	/**
	 * When blue player is a network player, then this is his/her/its port.
	 */
	public static int PLAYER_BLUE_PORT;

	/**
	 * When blue player is human player, decide between textbased user interface
	 * ("tui") or graphical user interface ("gui")
	 */
	public static String BLUE_INPUT_METHOD;

	//// PARAMETERS FOR MONTE CARLO PLAYER CONSTRUCTOR /////////////////////////////////////
	/**
	 * An attribute for MonteCarloPlayer-constructor The maximal number of moves
	 * to calculate per simulation.
	 */
	public static int PRESIM_RANGE;

	/**
	 * An attribute for MonteCarloPlayer-constructor The number of simulations
	 * for each move.
	 */
	public static int SIMULATIONS;

	/**
	 * An attribute for MonteCarloPlayer-constructor The number of random sites
	 * to calculate the move for.
	 */
	public static int POTENTIAL_SITES;

	/**
	 * An attribute for MonteCarloPlayer-constructor The scaling value of
	 * simulations in dependency of own-position-weight.
	 */
	public static int SIM_SCALING;

	/**
	 * An attribute for MonteCarloPlayer-constructor The randomness factor in
	 * calculations.
	 */
	public static int RANDOM_ACTIVATION_DIVISOR;

	/**
	 * An attribute for MonteCarloPlayer-constructor The initial number of taken
	 * moves to simulate with.
	 */
	public static int ADVANCED_PRECALC_MOVES;

	/**
	 * An attribute for MonteCarloPlayer-constructor The percentage offset of
	 * StrategicPlayer's move.
	 */
	public static int STP_MOVE_OFFSET;
	////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This is the main-method of the whole project. Here all the given command
	 * line parameters were checked and the game will be initialized. Then there
	 * is a main loop. If there is at least one remote player, the main loop
	 * only will display and update the game screen. Otherwise the players and
	 * the game board will be initialzed. Then the main loop asks for the next
	 * moves from all available player types.
	 *
	 * @param args A list of parameters to start the game.
	 */
	public static void main(String[] args) {

		//parameters will be read and used here with aid of ArgumentParser
		try {
			ArgumentParser arguments = new ArgumentParser(args);
			//IMPORTANT!
			//If help flag is set, then all other code will not be executed!
			try {
				HELP = arguments.isHelp();
			} catch (ArgumentParserException ape) {
				//error with set help flag, false will be used
				HELP = false;
			}
			if (HELP) {
				try {
					File helpFile = new File("argumentStandards.txt");
					Scanner s = new Scanner(helpFile);
					String line;
					try {
						while ((line = s.nextLine()) != null) {
							System.out.println(line);
						}
					} catch (NoSuchElementException nsee) {
						//unimportant
					}
					System.exit(0);
				} catch (IOException ioe) {
					System.err.println("Help file not supported.");
					System.exit(1);
				}
			}
			try {
				String mode = arguments.getNetworkMode();
				if (mode.equals("fullServer")) {
					NETWORK_MODE = RMIMode.FullServer;
				} else if (mode.equals("halfServer")) {
					NETWORK_MODE = RMIMode.HalfServer;
				} else {//standard case "RMIMode.Client"
					//default mode "client" requires two players -> otherwise standards
					NETWORK_MODE = RMIMode.Client;
				}
			} catch (ArgumentParserException ape) {
				//error with set networkMode, client will be used
				NETWORK_MODE = RMIMode.Client;
			}
			try {
				DEBUG = arguments.isDebug();
			} catch (ArgumentParserException ape) {
				//error with set debug flag, false will be used
				DEBUG = false;
			}
			try {
				SIZE = arguments.getSize();
			} catch (ArgumentParserException ape) {
				//No size set, standard will be used
				SIZE = 7;
			}
			try {
				MOVE_SLEEP_TIME = arguments.getDelay();
			} catch (ArgumentParserException ape) {
				//No delay set, standard will be used
				MOVE_SLEEP_TIME = 300;
			}
			//configure red player (start player)
			try {
				//tui is always executed
				RED_INPUT_METHOD = "tui";
				PLAYER_TYPE_RED = arguments.getRed();
				// if player is remote player, use ip setting and port setting from command arguments
				if (PLAYER_TYPE_RED == PlayerType.Remote) {
					try {
						PLAYER_RED_IP = arguments.getRedIP();
						PLAYER_RED_PORT = arguments.getRedPort();
					} catch (ArgumentParserException ape) {
						PLAYER_RED_IP = "localhost";
						PLAYER_RED_PORT = 80;
					}
				} else if (PLAYER_TYPE_RED == PlayerType.Human) {
					try {
						RED_INPUT_METHOD = arguments.getRedInputMethod();
					} catch (ArgumentParserException ape) {
						//no red playerInputMethod was set, standard will be tui
						RED_INPUT_METHOD = "tui";
					}
				}
			} catch (ArgumentParserException ape) {
				//no red blue player was set, human will be set
				PLAYER_TYPE_RED = PlayerType.Human;
			}
			//configure blue player
			try {
				//tui is always executed
				BLUE_INPUT_METHOD = "tui";
				PLAYER_TYPE_BLUE = arguments.getBlue();
				// if player is remote player, use ip setting and port setting from command arguments
				if (PLAYER_TYPE_BLUE == PlayerType.Remote) {
					try {
						PLAYER_BLUE_IP = arguments.getBlueIP();
						PLAYER_BLUE_PORT = arguments.getBluePort();
					} catch (ArgumentParserException ape) {
						PLAYER_BLUE_IP = "localhost";
						PLAYER_BLUE_PORT = 80;
					}
				} else if (PLAYER_TYPE_BLUE == PlayerType.Human) {
					try {
						BLUE_INPUT_METHOD = arguments.getBlueInputMethod();
					} catch (ArgumentParserException ape) {
						//no blue playerInputMethod was set, standard will be tui
						BLUE_INPUT_METHOD = "tui";
					}
				}
			} catch (ArgumentParserException ape) {
				//no blue player was set, human will be set
				PLAYER_TYPE_BLUE = PlayerType.Human;
			}
			//// MONTE CARLO PLAYER PARAMETERS /////////////////////////////////////////
			//no player-type decision here because MonteCarlo could be different types
			//parameters have no upper limit
			try {
				PRESIM_RANGE = arguments.getPresimRange();
				if (PRESIM_RANGE < 0) {
					throw new ArgumentParserException("presimRange-parameter outof bounds");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				PRESIM_RANGE = 50;
			}
			try {
				SIMULATIONS = arguments.getSimulations();
				if (SIMULATIONS < 0) {
					throw new ArgumentParserException("simulation-parameter out of bounds");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				SIMULATIONS = 20;
			}
			try {
				POTENTIAL_SITES = arguments.getPotentialSites();
				if (POTENTIAL_SITES < 0) {
					throw new ArgumentParserException("potentialSites-parameter out of bounds");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				POTENTIAL_SITES = 50;
			}
			try {
				SIM_SCALING = arguments.getSimScaling();
				if (SIM_SCALING < 0) {
					throw new ArgumentParserException("simScaling-parameter out of bounds");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				SIM_SCALING = 300;
			}
			try {
				RANDOM_ACTIVATION_DIVISOR = arguments.getRandomActivationDivisor();
				if (RANDOM_ACTIVATION_DIVISOR < 0) {
					throw new ArgumentParserException("randomActivationDivisor-parameter out of bounds.");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				RANDOM_ACTIVATION_DIVISOR = 10;
			}
			try {
				ADVANCED_PRECALC_MOVES = arguments.getAdvancedPrecalcMoves();
				if (ADVANCED_PRECALC_MOVES < 0) {
					throw new ArgumentParserException("advancedPrecalcMoves-parameter out of bounds.");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				ADVANCED_PRECALC_MOVES = 10;
			}
			try {
				STP_MOVE_OFFSET = arguments.getSTPMoveOffset();
				if (STP_MOVE_OFFSET < 0) {
					throw new ArgumentParserException("stpMoveOffset-parameter out of bounds.");
				}
			} catch (ArgumentParserException ape) {
				//set standard value
				STP_MOVE_OFFSET = 0;
			}
			//// END OF MONTE CARLO PLAYER PARAMETERS //////////////////////////////////
		} catch (ArgumentParserException ape) {
			System.err.println("Error while reading arguments!");
			System.err.println("Check your system's charset!");
			System.err.println("Game will terminate.");
			System.exit(1);
		}
		try {
			//make board
			HexagonalBoard gameBoard = new HexagonalBoard(SIZE);
			//make window
			Graphical window = new Graphical();
			//make both players
			//create requestable-objects to give them to the players
			//decide between tui and gui
			Requestable requestRed = null;
			Requestable requestBlue = null;
			if (PLAYER_TYPE_RED == PlayerType.Human) {
				if (RED_INPUT_METHOD.equals("gui")) {
					requestRed = (Requestable) (window);
				} else {//tui mode
					requestRed = new Request(PLAYER_TYPE_RED);
				}
			} else {
				//Not of interest
			}
			if (PLAYER_TYPE_BLUE == PlayerType.Human) {
				if (BLUE_INPUT_METHOD.equals("gui")) {
					requestBlue = (Requestable) (window);
				} else {//tui mode
					requestBlue = new Request(PLAYER_TYPE_BLUE);
				}
			} else {
				//Not of interest
			}
			//make players
			Player playerRed = makePlayer(PLAYER_TYPE_RED, requestRed);
			Player playerBlue = makePlayer(PLAYER_TYPE_BLUE, requestBlue);
			//make variables for shorter code lines
			PlayerType r = PLAYER_TYPE_RED;
			PlayerType b = PLAYER_TYPE_BLUE;
			PlayerType R = PlayerType.Remote;
			if (NETWORK_MODE == RMIMode.FullServer) {
				window.setNetworkMode(NETWORK_MODE);
				//server will be started to search for two players from RMIServerAcceptorModule
				RMIServerAcceptor.setSize(SIZE);
				RMIServerAcceptor.startRMIServer();
				//normal game loop
			} else if (NETWORK_MODE == RMIMode.HalfServer) {
				window.setNetworkMode(NETWORK_MODE);
				//server will be started to search for two players
				RMIServerAcceptor.setSize(SIZE);
				RMIServerAcceptor.startRMIServer();
				
				RMIClientAcceptor rca = null;
				PlayerColor pc = PlayerColor.Red;
				
				//add a player on local way, other will be added with RMIServerAcceptorModule
				if (r == R && b != R) {
					//red is remote, so use redIP and redPort for playerBlue
					rca = new RMIClientAcceptor(playerBlue);
					pc = rca.loginAtGameServer(playerBlue, PLAYER_RED_IP, PLAYER_RED_PORT);
				} else if (b == R && r != R) {
					//blue is remote, so use blueIP and bluePort for playerRed
					rca = new RMIClientAcceptor(playerRed);
					pc = rca.loginAtGameServer(playerRed, PLAYER_BLUE_IP, PLAYER_BLUE_PORT);
				} else if (b == R && r == R) {//too much (two) remote players
					System.err.println("Error! HalfServer with two remote players.");
					System.exit(1);
				} else {//rest case: no remote players
					System.err.println("Error! HalfServer without remote players.");
					System.exit(1);
				}
				window.setPlayerColor(pc);
				while(rca.viewer() == null) {
					Thread.yield();
				}
				//server will be started in new thread when second player is there
				showPassiveGameboard(rca.viewer(), window);
				//client mode (RMIMode.Client) and default mode 
			} else if (r == R && b == R) {//if both players are network players -> illegal
				System.err.println("Max. one remote player allowed! Use a server!");
				System.exit(1);
			} else if (r == R || b == R) {//client mode with exactly one network player
				window.setNetworkMode(RMIMode.Client);
				//load server
				//connect to the rmi-game-logic-server and transmit own address
				//and rmi server object
				Player current;
				PlayerColor pc = PlayerColor.Red;
				
				RMIClientAcceptor rca = null;
				
				if (b != R) {
					//blue is the local player, so use the blue player object
					current = playerBlue;
					rca = new RMIClientAcceptor(current);
					pc = rca.loginAtGameServer(current, PLAYER_RED_IP, PLAYER_RED_PORT);
				} else {//(r != R)
					//red is the local player, so use the red player object
					current = playerRed;
					rca = new RMIClientAcceptor(current);
					pc = rca.loginAtGameServer(current, PLAYER_BLUE_IP, PLAYER_BLUE_PORT);
				}
				window.setPlayerColor(pc);
				while (rca.viewer() == null) {
					Thread.yield();
				}
				showPassiveGameboard(rca.viewer(), window);
			} else {//normal local game -> normal local game loop
				window.setNetworkMode(null);//local game
				window.setPlayerColor(PlayerColor.Red);
				window.setGameBoard(gameBoard);
				playerRed.init(SIZE, PlayerColor.Red);
				playerBlue.init(SIZE, PlayerColor.Blue);
				Status status = Status.Ok;
				Move m;
				//main loop for two human players
				while (status == Status.Ok) {
					//red player
					m = playerRed.request();
					gameBoard.make(m);
					status = gameBoard.getStatus();
					playerRed.confirm(status);
					playerBlue.update(m, status);
					window.update(gameBoard.viewer());
					Thread.sleep(MOVE_SLEEP_TIME);
					//blue player
					if (status == Status.Ok) {
						m = playerBlue.request();
						gameBoard.make(m);
						status = gameBoard.getStatus();
						playerBlue.confirm(status);
						playerRed.update(m, status);
						window.update(gameBoard.viewer());
						Thread.sleep(MOVE_SLEEP_TIME);
					}
				}
			}
		} catch (RemoteException re) {
			System.err.println("Remote error in main loop!");
			re.printStackTrace();
			System.exit(1);
		} catch (InterruptedException ie) {
			System.err.println("Interruption error in main loop!");
			ie.printStackTrace();
			System.exit(1);
		} catch (Exception exc) {
			System.err.println("Any other exception in main");
			exc.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * This method can be called in passive game mode to constantly show the
	 * game board
	 *
	 * @param viewer The game board to show on screen
	 * @param window The graphical place to show the game board
	 */
	private static void showPassiveGameboard(Viewer viewer, Graphical window) {
		//in network mode, the game only updates the screen
		while (viewer.getStatus() == Status.Ok) {
			window.update(viewer);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				System.err.println("Terminating the game.");
				System.exit(1);
			}
		}
	}

	/**
	 * A helper method to make build the player objects in aid of their
	 * specified constructors
	 *
	 * @param p Any type of player
	 * @param r A request object for human players
	 * @throws RemoteException Is thrown when NetworkPlayer fails
	 * @return A player object
	 */
	private static Player makePlayer(PlayerType p, Requestable r) throws RemoteException {
		if (p == PlayerType.Human) {
			return new HumanPlayer(r);
		} else if (p == PlayerType.Remote) {
			return new NetworkPlayer(new RandomPlayer());
		} else if (p == PlayerType.RandomAI) {
			return new RandomPlayer();
		} else if (p == PlayerType.SimpleAI) {
			return new WinningComputerPlayer();
		} else if (p == PlayerType.ExtendedAI) {
			return new StrategicComputerPlayer();
		} else if (p == PlayerType.UpgradedAI) {
			//return new MonteCarloPlayer();
			return new MonteCarloPlayer(PRESIM_RANGE, SIMULATIONS, POTENTIAL_SITES, SIM_SCALING, RANDOM_ACTIVATION_DIVISOR, ADVANCED_PRECALC_MOVES, STP_MOVE_OFFSET);
		} else if (p == PlayerType.EnhancedAI) {
			System.err.println("Not implemented yet!");
			System.exit(1);
		} else if (p == PlayerType.AdvancedAI) {
			System.err.println("Not implemented yet!");
			System.exit(1);
		} else {
			System.err.println("Unknown player type detected!");
			System.exit(1);
		}
		return null;
	}
}
