package nowhere2gopp.network;

import java.lang.NullPointerException;
import java.net.InetAddress;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ExportException;
import java.rmi.server.RemoteServer;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import nowhere2gopp.base.NowhereToGoe;
import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Viewable;
import nowhere2gopp.preset.Viewer;

/**
 * This is the rmi-server for the client side of the whole game
 *
 * @author Maurice Mueller
 * @since 2019-07-06
 * @version 1.0
 */
public class RMIClientAcceptor implements Viewable {

	/**
	 * This constant is made for the game to use an united constant instead of
	 * any other port number. When you always use this constant, you are shure
	 * to use the right one.
	 */
	public static final int STD_PORT = Registry.REGISTRY_PORT;

	/**
	 * This is the port where the client offers his server.
	 */
	private int OWN_PORT = -1;

	RMIClientAcceptorModule rcam = null;

	/**
	 * A standard constuctor for an RMIClient
	 *
	 * @throws RemoteException in case of remote error
	 */
	public RMIClientAcceptor() throws RemoteException {
		startRMIClient(null);
	}

	/**
	 * A constructor to build an object with plyer object
	 *
	 * @param p A client/player object
	 * @throws RemoteException in case of remote error
	 */
	public RMIClientAcceptor(Player p) throws RemoteException {
		startRMIClient(p);
	}

	/**
	 * Starts the rmi client to offer moves for the game server. The given
	 * Player p is the local player on the client's side
	 *
	 * @param p A client/player object
	 * @throws RemoteException in case of remote error
	 */
	public void startRMIClient(Player p) throws RemoteException {
		if (p != null) {
			//create registry
			//create accepting module
			rcam = new RMIClientAcceptorModule(p);
			Player rcai = (Player) (UnicastRemoteObject.exportObject(rcam, 0));
			//set standard log output to standard output
			RemoteServer.setLog(System.out);
			Registry registry = LocateRegistry.getRegistry();
			try {
				int freePort = Registry.REGISTRY_PORT + 2;
				boolean todo = true;
				while (true) {
					//In case of exceptions the while-loop will repeated with the next
					//port number until there is a free port number
					try {
						registry = LocateRegistry.createRegistry(freePort);//standard port 1099
						System.out.println("Created client registry at port: " + freePort);
						registry.bind("clientAcceptor:" + freePort, rcai);
						System.out.println("clientAcceptor:" + freePort + " bound");
						break;
					} catch (ExportException ee) {
						if (NowhereToGoe.DEBUG) {
							System.err.println("ExportException in creating registry!");
							//ee.printStackTrace();
						}
					} catch (AlreadyBoundException abe) {
						if (NowhereToGoe.DEBUG) {
							System.err.println("AlreadyBoundException in creating registry!");
							//abe.printStackTrace();
						}
					} catch (AccessException ae) {
						if (NowhereToGoe.DEBUG) {
							System.err.println("AccessException in creating registry!");
							//ae.printStackTrace();
						}
					}
					if (freePort >= 1102) {
						System.err.println("Can't bind clientAcceptor to ports between " + STD_PORT + " and " + freePort + ".");
						System.exit(1);
					}
					freePort++;
				}
				if (NowhereToGoe.DEBUG) {
					System.out.println("Bound clientAcceptor to port: " + freePort + "!");
				}
				OWN_PORT = freePort;

			} catch (RemoteException re) {
				System.err.println("Error in creating registry!");
				re.printStackTrace();
				System.exit(1);
			}

		}
	}

	/**
	 * This method is used by a player/client program to add a remote-player to
	 * a server
	 *
	 * @param p The player obeject to use
	 * @param ip The ip of server
	 * @param port The port number of server
	 * @throws RemoteException in case of remote-error
	 * @return the player color of this player
	 */
	public PlayerColor loginAtGameServer(Player p, String ip, int port) throws RemoteException {
		PlayerColor pc = PlayerColor.Red;
		//implementation of client's registry code
		try {
			Registry registry = LocateRegistry.getRegistry(ip, port);
			try {
				//System.out.println(registry.lookup("serverAcceptor").getClass().getName());
				//System.out.println(registry.lookup("serverAcceptor").getClass().getSimpleName());
				RMIServerAcceptorInterface module = (RMIServerAcceptorInterface) Naming.lookup("rmi://" + ip + ":" + port + "/serverAcceptor");
				pc = module.addPlayer(p, InetAddress.getLocalHost().getHostAddress(), OWN_PORT);
			} catch (NotBoundException nbe) {
				//unknown error
				System.err.println("NotBoundException in logging in into game server!");
				System.err.println("ip: " + ip + " port: " + port + "own_port: " + OWN_PORT);
				nbe.printStackTrace();
				System.exit(1);
			} catch (Exception e) {
				//unknown error
				System.err.println("Exception in logging in into game server!");
				System.err.println("ip: " + ip + " port: " + port + "own_port: " + OWN_PORT);
				e.printStackTrace();
				System.exit(1);
			}
		} catch (RemoteException re) {
			//remote error
		}
		return pc;
	}

	@Override
	public Viewer viewer() {
		if (rcam == null) {
			return null;
		}
		return this.rcam.viewer();
	}

}
