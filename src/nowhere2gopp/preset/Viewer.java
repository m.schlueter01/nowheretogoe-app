package nowhere2gopp.preset;

import java.util.*;
import nowhere2gopp.board.HexagonalBoard.GamePhase;

public interface Viewer {
    PlayerColor getTurn();
    int getSize();
    Status getStatus();

    /** site blocked by the agent of given color */
    Site getAgent(final PlayerColor color);

    /** unbroken links */
    Collection<SiteSet> getLinks();
    
    /**
     * returns number of Sites
     */
    int getNumberOfPlaces();
	
	/**
	 * returns the Phase
	 */
	GamePhase getPhase();
}
