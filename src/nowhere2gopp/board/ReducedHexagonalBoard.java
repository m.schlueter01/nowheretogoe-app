package nowhere2gopp.board;

import nowhere2gopp.board.HexagonalBoard;
import nowhere2gopp.preset.Move;
import nowhere2gopp.preset.MoveType;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Status;

/**
 * HexagonalBoard with a reduced make function only for simulation purposes
 */
public class ReducedHexagonalBoard extends HexagonalBoard {

	/**
	 * Constructor wich copys the state of an HexagonalBoard
	 * @param b board to copy
	 */
	public ReducedHexagonalBoard(HexagonalBoard b) {
		super(b);
	}

	/**
	 * reduced make function which only does nessessary things
	 * @param move Move to do on board
	 * @throws IllegalStateException actually not thrown in this class
	 */
	@Override
	public void make(Move move) throws IllegalStateException {

		if (move.getType() == MoveType.End) {
			// set the winning player
			if (getTurn() == PlayerColor.Blue) {
				state = Status.RedWin;
			} else {
				state = Status.BlueWin;
			}

		} else {
			// move the agent who has its turn
			if (getTurn() == PlayerColor.Red) {
				agentRed = move.getAgent().getSecond();
			} else {
				agentBlue = move.getAgent().getSecond();
			}

			// remove the link
			getLinks().remove(move.getLink());

		}

		// Set next players turn
		plTurn = getNextPl(getTurn());

		// increase the Round counter
		roundHalf++;
	}
}
