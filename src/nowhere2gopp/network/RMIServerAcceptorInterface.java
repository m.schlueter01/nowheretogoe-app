package nowhere2gopp.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

import nowhere2gopp.preset.Player;
import nowhere2gopp.preset.PlayerColor;

/**
 * This is an interface to offer a method to add a client to a server. Therefor the client knows the server ip
 * and port and uses the offered RMI-module by the game-logic server.
 *
 * @author Maurice Mueller
 * @since 2019-07-03
 * @version 1.0
 */
public interface RMIServerAcceptorInterface extends Remote {
	/**
	 * This method is used by a player/client program to add a remote-player to a server
	 *
	 * @param p The player object to use as remote-player
	 * @param ip The ip address where the server can reach the client-RMI-server
	 * @param port The port number where the server can reach the client-RMI-server
	 * @return a color of type PlayerColor defined for a player
	 * @throws RemoteException in case of remote-error
	 */
	public PlayerColor addPlayer(Player p, String ip, int port) throws RemoteException;
}
